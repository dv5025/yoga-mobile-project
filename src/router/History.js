import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ViewAllHistories from 'src/screens/History/ViewAllHistories/index';
import ViewAtDate from 'src/screens/History/ViewAtDate/index';

const Stack = createStackNavigator();

const History = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="ViewAllHistories">
      <Stack.Screen name="ViewAllHistories" component={ViewAllHistories} />
      <Stack.Screen name="ViewAtDate" component={ViewAtDate} />
    </Stack.Navigator>
  );
};

export default History;
