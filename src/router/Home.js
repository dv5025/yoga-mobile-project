import React from 'react';
import {Icon} from 'native-base';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Poses from './Poses';
import Classes from './YogaClasses';
import Community from './Community';
import History from './History';
import More from './More';

const Tab = createBottomTabNavigator();

const Home = () => {
  return (
    <Tab.Navigator
      initialRouteName="Classes"
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          let iconName;
          if (route.name === 'Poses') {
            iconName = 'baby';
          } else if (route.name === 'Classes') {
            iconName = 'spa';
          } else if (route.name === 'Community') {
            iconName = 'account-supervisor-circle';
          } else if (route.name === 'History') {
            iconName = 'history';
          } else if (route.name === 'More') {
            iconName = 'menu';
          }
          return (
            <Icon
              type="MaterialCommunityIcons"
              name={iconName}
              size={size}
              style={{color}}
            />
          );
        },
      })}
      tabBarOptions={{
        activeTintColor: '#78D1D2',
        inactiveTintColor: '#9F9F9F',
        keyboardHidesTabBar: true,
      }}>
      <Tab.Screen name="Classes" component={Classes} />
      <Tab.Screen name="Poses" component={Poses} />
      <Tab.Screen name="Community" component={Community} />
      <Tab.Screen name="History" component={History} />
      <Tab.Screen name="More" component={More} />
    </Tab.Navigator>
  );
};

export default Home;
