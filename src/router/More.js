import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import MoreScreen from 'src/screens/More/Menu/index';
import Profile from 'src/screens/More/Profile/index';
import ChangePass from 'src/screens/More/ChangePass/index';
import EditProfile from 'src/screens/More/EditProfile/index';
import ChangeProfileImage from 'src/screens/More/ChangeProfileImage/index';
import AboutApp from 'src/screens/More/AboutApp/index';

const Stack = createStackNavigator();

const More = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      options={TransitionPresets.SlideFromRightIOS}
      initialRouteName="More">
      <Stack.Screen name="More" component={MoreScreen} />
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={TransitionPresets.SlideFromRightIOS}
      />
      <Stack.Screen
        name="ChangePass"
        component={ChangePass}
        options={TransitionPresets.SlideFromRightIOS}
      />
      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={TransitionPresets.SlideFromRightIOS}
      />
      <Stack.Screen
        name="ChangeProfileImage"
        component={ChangeProfileImage}
        options={TransitionPresets.SlideFromRightIOS}
      />
      <Stack.Screen
        name="AboutApp"
        component={AboutApp}
        options={TransitionPresets.SlideFromRightIOS}
      />
    </Stack.Navigator>
  );
};

export default More;
