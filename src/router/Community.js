import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import ViewPosts from 'src/screens/Community/ViewPosts/index';
import ViewComment from 'src/screens/Community/ViewComment/index';
import AddPost from 'src/screens/Community/AddPost/index';
import EditPost from 'src/screens/Community/EditPost/index';

const Stack = createStackNavigator();

const Community = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="ViewPosts">
      <Stack.Screen name="ViewPosts" component={ViewPosts} />
      <Stack.Screen
        name="ViewComment"
        component={ViewComment}
        options={TransitionPresets.SlideFromRightIOS}
      />
      <Stack.Screen name="AddPost" component={AddPost} />
      <Stack.Screen name="EditPost" component={EditPost} />
    </Stack.Navigator>
  );
};

export default Community;
