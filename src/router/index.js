import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from 'src/router/Home';
import Login from 'src/screens/Auth/Login/index';
import Register from 'src/screens/Auth/Register/index';
import {useSelector} from 'react-redux';
import DoingYoga from 'src/screens/YogaClasses/DoingYoga/index';

const Stack = createStackNavigator();

export default () => {
  const {token} = useSelector((state) => state.auth);

  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName={token === '' ? 'Login' : 'Home'}>
      {token === '' ? (
        <>
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
        </>
      ) : (
        <>
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="DoingYoga" component={DoingYoga} />
        </>
      )}
    </Stack.Navigator>
  );
};
