import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ViewAllClasses from 'src/screens/YogaClasses/ViewAllYogaClass/index';
import SelectLevel from 'src/screens/YogaClasses/SelectLevel/index';
import ShowPoses from 'src/screens/YogaClasses/ShowPoses/index';
import AddClass from 'src/screens/YogaClasses/CreateCustomClass/index';

const Stack = createStackNavigator();

const YogaClasses = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="ViewAllClasses">
      <Stack.Screen name="ViewAllClasses" component={ViewAllClasses} />
      <Stack.Screen name="SelectLevel" component={SelectLevel} />
      <Stack.Screen name="ShowPoses" component={ShowPoses} />
      <Stack.Screen name="AddClass" component={AddClass} />
    </Stack.Navigator>
  );
};

export default YogaClasses;
