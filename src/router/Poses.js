import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ViewAllPoses from 'src/screens/Poses/ViewAllPoses/index';
import ViewPose from 'src/screens/Poses/ViewPose';

const Stack = createStackNavigator();

const Poses = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="ViewAllPoses">
      <Stack.Screen name="ViewAllPoses" component={ViewAllPoses} />
      <Stack.Screen name="ViewPose" component={ViewPose} />
    </Stack.Navigator>
  );
};

export default Poses;
