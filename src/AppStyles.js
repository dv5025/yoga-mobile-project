import {StyleSheet} from 'react-native';

export const colors = {
  color_1: '#75BDE0',
  color_2: '#78D1D2',
  color_3: '#97DBAE',
  lightgray: '#F2F2F7',
  text_1: '#929292',
};

const appStyles = StyleSheet.create({
  headline: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#000000',
  },
  headlineDescription: {
    fontSize: 13,
    fontWeight: 'normal',
    color: '#929292',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#4F4F4F',
  },
  subtitle: {
    color: '#4D4D4D',
    fontWeight: 'bold',
    fontSize: 15,
  },
  text: {
    fontSize: 12,
    fontWeight: 'normal',
    color: '#2F2F2F',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  fulfill: {
    flex: 1,
  },
  textCenter: {
    textAlign: 'center',
  },
  scrollView: {
    flexGrow: 1,
  },
});

export default appStyles;
