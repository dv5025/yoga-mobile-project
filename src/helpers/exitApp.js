import {Alert} from 'react-native';

const exitApp = (BackHandler) => {
  Alert.alert('Hold on!', 'Are you sure you want to exit the app?', [
    {
      text: 'Cancel',
      onPress: () => null,
      style: 'cancel',
    },
    {text: 'YES', onPress: () => BackHandler.exitApp()},
  ]);
  return true;
};

export default exitApp;
