export const isInputError = (touchFormik, error, manuallyTouch) => {
  return touchFormik
    ? touchFormik && error
      ? true
      : false
    : manuallyTouch && error
    ? true
    : false;
};

export const getErrorMessage = (touchFormik, errorMes, manuallyTouch) => {
  return touchFormik
    ? touchFormik && errorMes
      ? errorMes
      : ''
    : manuallyTouch && errorMes
    ? errorMes
    : '';
};
