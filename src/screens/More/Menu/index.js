import React, {useCallback} from 'react';
import {View, StatusBar, BackHandler} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useFocusEffect} from '@react-navigation/native';
import exitApp from 'src/helpers/exitApp';
import styles from './styles';
import appStyles from 'src/AppStyles';
import SimpleHeader from 'src/components/SimpleHeader';
import Menu from './Menu';

export default () => {
  useFocusEffect(
    useCallback(() => {
      const backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        () => exitApp(BackHandler),
      );

      return () => backHandler.remove();
    }, []),
  );

  return (
    <SafeAreaView style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <View style={styles.wrapHeader}>
        <SimpleHeader title="More" />
      </View>
      <Menu />
    </SafeAreaView>
  );
};
