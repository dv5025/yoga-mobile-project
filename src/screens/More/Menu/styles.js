import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: colors.lightgray,
  },
  wrapHeader: {
    paddingHorizontal: 15,
    marginBottom: 10,
  },
  rightIcon: {
    color: colors.color_1,
    fontSize: 18,
  },
});

export default styles;
