import React from 'react';
import {Icon} from 'native-base';
import Divider from 'src/components/ListDivider';
import {ScrollView, Alert} from 'react-native';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import ListItemWithIcon from '../../../components/ListItemWithIcon';
import {logout} from '../../../actions/AuthAction';
import {useDispatch, useSelector} from 'react-redux';
import email from 'react-native-email';
import authApi from '../../../api/auth';

const Menu = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {token} = useSelector((state) => state.auth);

  const onPressLogout = () => {
    authApi
      .logout(token)
      .then(() => {
        dispatch(logout());
      })
      .catch((err) => Alert.alert(err));
  };

  const onPressProfile = () => {
    navigation.navigate('Profile');
  };

  const onPressAboutApp = () => {
    navigation.navigate('AboutApp');
  };

  const handleEmail = () => {
    const to = ['sit-tipon@hotmail.com']; // string or array of email addresses
    email(to, {
      subject: 'Yoga Trainer App',
    }).catch(console.error);
  };

  return (
    <ScrollView>
      <ListItemWithIcon
        icon="account-circle"
        iconColor="#FF9501"
        name="Profile"
        rightIcon={
          <Icon
            active
            type="MaterialCommunityIcons"
            name="chevron-right"
            style={styles.rightIcon}
          />
        }
        onPress={onPressProfile}
      />

      <Divider width={20} />

      <ListItemWithIcon
        icon="information"
        iconColor="#2C9AFF"
        name="About app"
        onPress={onPressAboutApp}
      />

      <Divider width={5} />

      <ListItemWithIcon
        icon="email"
        iconColor="#2C9AFF"
        name="Contact us"
        onPress={handleEmail}
      />

      <Divider width={20} />

      <ListItemWithIcon
        icon="logout"
        iconColor="#FF6060"
        name="Log out"
        onPress={onPressLogout}
      />
    </ScrollView>
  );
};

export default Menu;
