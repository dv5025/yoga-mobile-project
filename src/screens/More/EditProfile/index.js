import React, {useState, useRef, Fragment} from 'react';
import {StatusBar, View, Alert} from 'react-native';
import styles from './styles';
import appStyles, {colors} from 'src/AppStyles';
import PrimaryHeader from 'src/components/PrimaryHeader';
import FormInput from 'src/components/FormInput';
import Button from 'src/components/Button';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useNavigation} from '@react-navigation/native';
import userApi from '../../../api/user';
import {useSelector} from 'react-redux';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {isInputError, getErrorMessage} from '../../../helpers/validation';

export default ({route}) => {
  const navigation = useNavigation();
  const {user} = route.params;
  const {token} = useSelector((state) => state.auth);
  const [inputTouched, setInputTouched] = useState({
    firstname: false,
    lastname: false,
  });

  const [isLoading, setIsLoading] = useState(false);

  const lastNameInput = useRef();

  const handleOnSave = (values) => {
    setIsLoading(true);
    userApi
      .editUserProfile(token, {
        firstname: values.firstname,
        lastname: values.lastname,
      })
      .then(() => {
        setIsLoading(false);
        Alert.alert('Saved');
        navigation.goBack();
      })
      .catch((err) => {
        setIsLoading(false);
        if (err.response) {
          console.log(err.response);
        } else if (err.request) {
          Alert.alert(err.request._response);
        } else {
          console.log(err);
        }
      });
  };

  const isDisableSaveButton = (errprMes, values) => {
    if (
      values.firstname === user.firstname &&
      values.lastname === user.lastname
    ) {
      return true;
    } else {
      return errprMes.lastname || errprMes.firstname ? true : false;
    }
  };

  const validationSchema = Yup.object().shape({
    firstname: Yup.string()
      .required('Please enter first name')
      .min(2, 'Firstname must have at least 2 characters '),
    lastname: Yup.string()
      .required('Please enter last name')
      .min(2, 'Lastname must have at least 2 characters '),
  });

  return (
    <View style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <PrimaryHeader
        backBtn={true}
        title="Edit Profile"
        subtitleStyle={true}
        containerStyle={styles.header}
      />
      <KeyboardAwareScrollView contentContainerStyle={appStyles.scrollView}>
        <View style={styles.contentContainer}>
          <Formik
            initialValues={{
              firstname: user.firstname,
              lastname: user.lastname,
            }}
            onSubmit={(values) => handleOnSave(values)}
            validationSchema={validationSchema}>
            {({handleChange, values, handleSubmit, errors, touched}) => (
              <Fragment>
                <View style={styles.form}>
                  <FormInput
                    label="First name"
                    placeholder="firstname"
                    returnKeyType="next"
                    value={values.firstname}
                    onChangeText={(text) => {
                      setInputTouched({...inputTouched, firstname: true});
                      handleChange('firstname')(text);
                    }}
                    onSubmitEditing={() => lastNameInput.current.focus()}
                    inputError={isInputError(
                      touched.firstname,
                      errors.firstname,
                      inputTouched.firstname,
                    )}
                    errorMessage={getErrorMessage(
                      touched.firstname,
                      errors.firstname,
                      inputTouched.firstname,
                    )}
                  />
                  <FormInput
                    ref={lastNameInput}
                    label="Last name"
                    placeholder="lastname"
                    value={values.lastname}
                    onChangeText={(text) => {
                      setInputTouched({...inputTouched, lastname: true});
                      handleChange('lastname')(text);
                    }}
                    inputError={isInputError(
                      touched.lastname,
                      errors.lastname,
                      inputTouched.lastname,
                    )}
                    errorMessage={getErrorMessage(
                      touched.lastname,
                      errors.lastname,
                      inputTouched.lastname,
                    )}
                  />
                </View>
                <View style={appStyles.center}>
                  <Button
                    gradient
                    colors={colors.pink_orange}
                    loading={isLoading}
                    label="Save"
                    containerStyle={styles.saveButton}
                    style={styles.button}
                    onPress={handleSubmit}
                    disabled={isDisableSaveButton(errors, values)}
                  />
                </View>
              </Fragment>
            )}
          </Formik>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};
