import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: '#FFFFFF',
  },
  header: {
    backgroundColor: '#FFFFFF',
  },
  contentContainer: {
    backgroundColor: colors.lightgray,
    flex: 1,
    paddingBottom: 20,
  },
  form: {
    backgroundColor: '#FFFFFF',
    margin: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 8,
  },
  saveButton: {
    width: 200,
  },
  button: {
    borderRadius: 100,
  },
});

export default styles;
