import React, {useCallback, useState} from 'react';
import {StatusBar, View, Alert} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import styles from './styles';
import appStyles from 'src/AppStyles';
import PrimaryHeader from 'src/components/PrimaryHeader';
import UserInfo from './UserInfo';
import Menu from './Menu';
import userApi from '../../../api/user';
import {useSelector} from 'react-redux';

export default () => {
  const {token} = useSelector((state) => state.auth);
  const [user, setUser] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useFocusEffect(
    useCallback(() => {
      getUserProfile();
    }, []),
  );

  const getUserProfile = () => {
    setIsLoading(true);
    userApi
      .getUserProfile(token)
      .then((res) => {
        setTimeout(() => {
          setIsLoading(false);
          setUser(res.data);
        }, 500);
      })
      .catch((err) => {
        setTimeout(() => {
          setIsLoading(false);
          Alert.alert(err);
        }, 500);
      });
  };

  return (
    <View style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <PrimaryHeader
        backBtn={true}
        title="Profile"
        containerStyle={styles.header}
      />
      <View style={styles.contentContainer}>
        <UserInfo user={user} loading={isLoading} />
        <Menu user={user} />
      </View>
    </View>
  );
};
