import React from 'react';
import {View, Image, Text} from 'react-native';
import styles from './styles';
import appStyles from 'src/AppStyles';
import {profileImageUrl} from '../../../api/index';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const UserInfo = ({user, loading}) => {
  const defaultImage = require('src/assets/user.png');

  return (
    <View style={[appStyles.center, styles.wrapUserInfo]}>
      {loading ? (
        <SkeletonPlaceholder>
          <View style={styles.profileImage} />
          <View style={[styles.wrapName, appStyles.center]}>
            <View style={{width: 120, height: 20, borderRadius: 4}} />
            <View
              style={{marginTop: 6, width: 80, height: 20, borderRadius: 4}}
            />
          </View>
        </SkeletonPlaceholder>
      ) : (
        <>
          <Image
            style={styles.profileImage}
            source={
              user.profileImage?.filename
                ? {
                    uri: `${profileImageUrl}/${user.profileImage.filename}`,
                  }
                : defaultImage
            }
          />
          <View style={[styles.wrapName, appStyles.center]}>
            <Text style={[appStyles.text, styles.name]}>
              {user.firstname + ' ' + user.lastname}
            </Text>
            <Text style={styles.username}>{'@' + user.username}</Text>
          </View>
        </>
      )}
    </View>
  );
};

export default UserInfo;
