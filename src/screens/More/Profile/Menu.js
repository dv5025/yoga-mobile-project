import React from 'react';
import {View} from 'react-native';
import {Icon} from 'native-base';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import ListItemWithIcon from 'src/components/ListItemWithIcon';
import Divider from 'src/components/ListDivider';

const Menu = ({user}) => {
  const navigation = useNavigation();

  return (
    <View>
      <ListItemWithIcon
        icon="image"
        iconColor="#585858"
        name="Change profile image"
        rightIcon={
          <Icon
            active
            type="MaterialCommunityIcons"
            name="chevron-right"
            style={styles.rightIcon}
          />
        }
        onPress={() => navigation.navigate('ChangeProfileImage')}
      />

      <Divider width={5} />

      <ListItemWithIcon
        icon="textbox-password"
        iconColor="#585858"
        name="Change password"
        rightIcon={
          <Icon
            active
            type="MaterialCommunityIcons"
            name="chevron-right"
            style={styles.rightIcon}
          />
        }
        onPress={() => navigation.navigate('ChangePass')}
      />

      <Divider width={5} />

      <ListItemWithIcon
        icon="account-box"
        iconColor="#585858"
        name="Edit Profile"
        rightIcon={
          <Icon
            active
            type="MaterialCommunityIcons"
            name="chevron-right"
            style={styles.rightIcon}
          />
        }
        onPress={() => navigation.navigate('EditProfile', {user})}
      />
    </View>
  );
};

export default Menu;
