import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: '#FFFFFF',
  },
  header: {
    backgroundColor: '#FFFFFF',
  },
  contentContainer: {
    backgroundColor: colors.lightgray,
    flex: 1,
  },
  wrapUserInfo: {
    paddingVertical: 30,
  },
  profileImage: {
    width: 120,
    height: 120,
    borderRadius: 200,
    borderWidth: 3,
    borderColor: '#FFFFFF',
  },
  wrapName: {
    marginTop: 10,
  },
  name: {
    fontSize: 16,
  },
  username: {
    color: '#AAAAAA',
    fontSize: 12,
  },
  rightIcon: {
    color: colors.color_1,
    fontSize: 18,
  },
});

export default styles;
