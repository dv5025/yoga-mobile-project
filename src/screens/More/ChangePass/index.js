import React, {useState, useRef, Fragment} from 'react';
import {StatusBar, View, Alert} from 'react-native';
import styles from './styles';
import appStyles, {colors} from 'src/AppStyles';
import PrimaryHeader from 'src/components/PrimaryHeader';
import FormInput from 'src/components/FormInput';
import Button from 'src/components/Button';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useNavigation} from '@react-navigation/native';
import userApi from '../../../api/user';
import {useSelector} from 'react-redux';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {isInputError, getErrorMessage} from '../../../helpers/validation';

export default () => {
  const navigation = useNavigation();
  const {token} = useSelector((state) => state.auth);
  const [inputTouched, setInputTouched] = useState({
    password: false,
    confirmPassword: false,
  });

  const [isLoading, setIsLoading] = useState(false);

  const [isShowPassword, setIsShowPassword] = useState(false);
  const [isShowConfirmPassword, setIsShowConfirmPassword] = useState(false);

  const confirmPasswordInput = useRef();

  const handleFormSubmit = (values) => {
    setIsLoading(true);
    userApi
      .editUserPassword(token, {
        password: values.password,
      })
      .then(() => {
        setIsLoading(false);
        Alert.alert('Password changed');
        navigation.goBack();
      })
      .catch((err) => {
        setIsLoading(false);
        if (err.response) {
          console.log(err.response);
        } else if (err.request) {
          Alert.alert(err.request._response);
        } else {
          console.log(err);
        }
      });
  };

  const isDisableSaveButton = (errprMes, values) => {
    if (values.password === '' || values.confirmPassword === '') {
      return true;
    } else {
      return errprMes.password || errprMes.confirmPassword ? true : false;
    }
  };

  const validationSchema = Yup.object().shape({
    password: Yup.string()
      .required('Please enter a password')
      .min(8, 'Password must have at least 8 characters '),
    confirmPassword: Yup.string()
      .oneOf([Yup.ref('password')], 'Password not match')
      .required('Confirm Password is required'),
  });

  return (
    <View style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <PrimaryHeader
        backBtn={true}
        title="Change password"
        subtitleStyle={true}
        containerStyle={styles.header}
      />
      <KeyboardAwareScrollView contentContainerStyle={appStyles.scrollView}>
        <View style={styles.contentContainer}>
          <Formik
            initialValues={{
              password: '',
              confirmPassword: '',
            }}
            onSubmit={(values) => handleFormSubmit(values)}
            validationSchema={validationSchema}>
            {({handleChange, values, handleSubmit, errors, touched}) => (
              <Fragment>
                <View style={styles.form}>
                  <FormInput
                    label="Password"
                    labelIcon="lock"
                    placeholder="password"
                    returnKeyType="next"
                    value={values.password}
                    onChangeText={(text) => {
                      setInputTouched({...inputTouched, password: true});
                      handleChange('password')(text);
                    }}
                    rightIcon={isShowPassword ? 'eye-off' : 'eye'}
                    rightIconOnPress={() => setIsShowPassword(!isShowPassword)}
                    secureTextEntry={!isShowPassword}
                    onSubmitEditing={() => confirmPasswordInput.current.focus()}
                    inputError={isInputError(
                      touched.password,
                      errors.password,
                      inputTouched.password,
                    )}
                    errorMessage={getErrorMessage(
                      touched.password,
                      errors.password,
                      inputTouched.password,
                    )}
                  />
                  <FormInput
                    ref={confirmPasswordInput}
                    label="Confirm password"
                    labelIcon="textbox-password"
                    placeholder="confirm password"
                    returnKeyType="next"
                    value={values.confirmPassword}
                    onChangeText={(text) => {
                      setInputTouched({...inputTouched, confirmPassword: true});
                      handleChange('confirmPassword')(text);
                    }}
                    rightIcon={isShowConfirmPassword ? 'eye-off' : 'eye'}
                    rightIconOnPress={() =>
                      setIsShowConfirmPassword(!isShowConfirmPassword)
                    }
                    secureTextEntry={!isShowConfirmPassword}
                    inputError={isInputError(
                      touched.confirmPassword,
                      errors.confirmPassword,
                      inputTouched.confirmPassword,
                    )}
                    errorMessage={getErrorMessage(
                      touched.confirmPassword,
                      errors.confirmPassword,
                      inputTouched.confirmPassword,
                    )}
                  />
                </View>
                <View style={appStyles.center}>
                  <Button
                    gradient
                    colors={colors.pink_orange}
                    loading={isLoading}
                    label="Save"
                    containerStyle={styles.saveButton}
                    style={styles.button}
                    onPress={handleSubmit}
                    disabled={isDisableSaveButton(errors, values)}
                  />
                </View>
              </Fragment>
            )}
          </Formik>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};
