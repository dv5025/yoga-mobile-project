import React from 'react';
import {View, Text, StatusBar, Image, TouchableOpacity} from 'react-native';
import styles from './styles';
import appStyles from 'src/AppStyles';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Icon} from 'native-base';
import {useNavigation} from '@react-navigation/native';
import Divider from 'src/components/ListDivider';

const AboutApp = () => {
  const navigation = useNavigation();

  return (
    <SafeAreaView style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <View style={styles.wrapBackIcon}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Icon
            type="MaterialCommunityIcons"
            name="chevron-left"
            style={styles.backIcon}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.wrapContent}>
        <Image source={require('src/assets/logo.png')} style={styles.logo} />
        <Divider width={32} />
        <Text style={styles.title}>About App</Text>
        <Divider width={8} />
        <Text style={styles.description}>
          Keep Yoga - Relax, Meditation & Daily Fitness provides health benefits
          for both your mind and body with 400+ asanas, 40+ yoga session plans,
          and 7 meditation courses. Lose weight, fully relax, sleep better,
          boost your immune system, and keep a calmer mind with the app.
        </Text>
        <Divider width={8} />
        <Text style={styles.description}>
          No matter what level you’re at, beginners or expert yogis, this is the
          best app for you to keep up with your practice at your own pace in the
          comfort of your own home. We provide different levels of yoga sessions
          for all users, so In Keep Yoga, you will have the perfect experience
          made just for you.
        </Text>
      </View>
      <View style={styles.wrapFooter}>
        <Icon
          type="MaterialCommunityIcons"
          name="spa"
          style={styles.footerIcon}
        />
        <Text style={styles.creator}>Created by Sittiphon Kongkaew</Text>
      </View>
    </SafeAreaView>
  );
};

export default AboutApp;
