import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: colors.lightgray,
  },
  wrapBackIcon: {
    marginLeft: 8,
    marginTop: 16,
  },
  backIcon: {
    fontSize: 35,
    color: '#5B5B5B',
  },
  wrapContent: {
    alignItems: 'center',
    flex: 1,
    marginTop: 32,
    marginHorizontal: 32,
  },
  logo: {
    height: 120,
    width: 120,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#4C4C4C',
  },
  description: {
    textAlign: 'center',
  },
  wrapFooter: {
    alignItems: 'center',
    paddingVertical: 8,
  },
  footerIcon: {
    color: '#C6C6C6',
  },
  creator: {
    fontSize: 12,
    color: '#C6C6C6',
  },
});

export default styles;
