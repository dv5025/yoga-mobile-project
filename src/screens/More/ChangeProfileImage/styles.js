import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: '#FFFFFF',
  },
  header: {
    backgroundColor: '#FFFFFF',
  },
  contentContainer: {
    backgroundColor: colors.lightgray,
    flex: 1,
    paddingBottom: 20,
  },
  saveButton: {
    width: 150,
  },
  button: {
    borderRadius: 100,
  },
  wrapImagePicker: {
    marginTop: 40,
    marginBottom: 20,
  },
});

export default styles;
