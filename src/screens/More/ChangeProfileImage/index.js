import React, {useState} from 'react';
import {StatusBar, View, Alert} from 'react-native';
import styles from './styles';
import appStyles, {colors} from 'src/AppStyles';
import PrimaryHeader from 'src/components/PrimaryHeader';
import Button from 'src/components/Button';
import ImagePicker from 'src/components/ImagePicker';
import {useNavigation} from '@react-navigation/native';
import userApi from '../../../api/user';
import {useSelector} from 'react-redux';

export default () => {
  const navigation = useNavigation();
  const {token} = useSelector((state) => state.auth);

  const [profileImage, setProfileImage] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const onSave = () => {
    setIsLoading(true);
    const formData = new FormData();
    formData.append('image', {
      uri: profileImage.path,
      name: 'profile.jpg',
      type: profileImage.mime,
    });
    userApi
      .editProfileImage(token, formData)
      .then((res) => {
        setTimeout(() => {
          setIsLoading(false);
          Alert.alert('Profile images updated');
          navigation.goBack();
        }, 500);
      })
      .catch((err) => {
        setTimeout(() => {
          setIsLoading(false);
          Alert.alert(err);
        }, 500);
      });
  };

  const isDisableSaveButton = () => {
    if (profileImage === '') {
      return true;
    } else {
      return false;
    }
  };

  return (
    <View style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <PrimaryHeader
        backBtn={true}
        title="Change profile image"
        subtitleStyle={true}
        containerStyle={styles.header}
      />
      <View style={styles.contentContainer}>
        <View style={appStyles.center}>
          <View style={styles.wrapImagePicker}>
            <ImagePicker
              source={
                profileImage === ''
                  ? require('src/assets/user.png')
                  : {
                      uri: profileImage.path,
                    }
              }
              onSelected={(res) => setProfileImage(res)}
            />
          </View>
          <Button
            gradient
            colors={colors.pink_orange}
            loading={isLoading}
            label="Save"
            containerStyle={styles.saveButton}
            style={styles.button}
            onPress={onSave}
            disabled={isDisableSaveButton()}
          />
        </View>
      </View>
    </View>
  );
};
