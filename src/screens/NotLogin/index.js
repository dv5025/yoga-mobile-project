import React from 'react';
import {Text, View} from 'react-native';
import styles from './styles';
import appStyles, {colors} from 'src/AppStyles';
import Button from 'src/components/Button';
import {useNavigation} from '@react-navigation/native';
import {Icon} from 'native-base';

export default () => {
  const navigation = useNavigation();

  return (
    <View style={[appStyles.center, styles.wrapContent]}>
      <Icon
        type="MaterialCommunityIcons"
        name="account-alert"
        style={styles.warningIcon}
      />
      <Text>Please log in before using this feature.</Text>
      <Button
        color={colors.color_3}
        label="Login"
        containerStyle={styles.loginBtnContainer}
        style={styles.loginBtn}
        onPress={() => navigation.navigate('Login')}
      />
    </View>
  );
};
