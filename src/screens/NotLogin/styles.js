import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  screenContainer: {
    backgroundColor: colors.lightgray,
  },
  wrapContent: {
    marginVertical: 40,
  },
  loginBtnContainer: {
    width: 120,
    marginTop: 32,
  },
  loginBtn: {
    borderRadius: 8,
    height: 40,
  },
  warningIcon: {
    color: '#A2A2A2',
    fontSize: 100,
  },
});

export default styles;
