import React from 'react';
import {StatusBar} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Header from './Header';
import styles from './styles';
import appStyles from 'src/AppStyles';
import PosesList from './PosesList';
import moment from 'moment';

export default ({route}) => {
  const {data} = route.params;

  return (
    <SafeAreaView style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <Header
        title={data.yogaClass.name}
        titleDes={data.levelName ? ' - ' + data.levelName : ''}
        subtitle={moment(data.created_at).format('DD-MM-YYYY HH:mm')}
      />
      <PosesList data={data.poses} />
    </SafeAreaView>
  );
};
