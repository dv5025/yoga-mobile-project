import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {colors} from 'src/AppStyles';
import {Icon} from 'native-base';
import {useNavigation} from '@react-navigation/native';

const Header = ({title, titleDes, subtitle}) => {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.wrapIcon}
        onPress={() => navigation.goBack()}>
        <Icon
          type="MaterialCommunityIcons"
          name="chevron-left"
          style={styles.icon}
        />
      </TouchableOpacity>
      <View style={styles.titleContainer}>
        <View style={styles.row}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.titleDes}>{titleDes}</Text>
        </View>
        <Text style={styles.subtitle}>{subtitle}</Text>
      </View>
      <View style={styles.wrapIcon} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    backgroundColor: '#FFFFFF',
    paddingVertical: 15,
    marginHorizontal: 8,
    marginTop: 10,
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  titleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: colors.color_1,
  },
  titleDes: {
    fontSize: 16,
    color: colors.color_1,
  },
  subtitle: {
    fontSize: 12,
  },
  icon: {
    color: colors.color_1,
    fontSize: 35,
  },
  wrapIcon: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 10,
    width: 60,
  },
  row: {
    flexDirection: 'row',
  },
});

export default Header;
