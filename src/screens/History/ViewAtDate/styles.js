import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: colors.lightgray,
  },
  flatlist: {
    marginHorizontal: 8,
  },
  card: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    paddingVertical: 10,
  },
  wrapPoseImage: {
    width: 90,
    height: 90,
    borderColor: colors.color_1,
    borderWidth: 2,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 30,
  },
  poseImage: {
    width: 60,
    height: 60,
  },
  poseName: {
    width: 250,
    fontWeight: 'bold',
    fontSize: 13,
  },
  time: {
    fontSize: 13,
    color: '#868686',
  },
});

export default styles;
