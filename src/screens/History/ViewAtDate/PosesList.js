import React from 'react';
import {FlatList, Image, TouchableOpacity} from 'react-native';
import {Text, View} from 'native-base';
import EmptyList from 'src/components/EmptyList';
import styles from './styles';
import appStyles from 'src/AppStyles';
import ListDivider from 'src/components/ListDivider';
import {poseImageUrl} from '../../../api/index';

const PosesList = ({data}) => {
  const renderItem = ({item}) => {
    const {poses} = item;
    return (
      <TouchableOpacity style={styles.card} activeOpacity={0.6}>
        <View style={styles.wrapPoseImage}>
          <Image
            source={{uri: `${poseImageUrl}/${poses.image.filename}`}}
            style={styles.poseImage}
          />
        </View>
        <View style={appStyles.fulfill}>
          <Text style={styles.poseName}>{poses.name}</Text>
          <Text style={styles.time}>{item.time + ' seconds'}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <FlatList
      data={data}
      renderItem={renderItem}
      keyExtractor={(item, index) => index + ''}
      ListEmptyComponent={<EmptyList />}
      style={styles.flatlist}
      ItemSeparatorComponent={() => <ListDivider />}
    />
  );
};

export default PosesList;
