import React from 'react';
import {FlatList, TouchableOpacity} from 'react-native';
import {Text, Icon, View} from 'native-base';
import EmptyList from 'src/components/EmptyList';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import ListDivider from 'src/components/ListDivider';
import moment from 'moment';
import SkeletonLoading from './SkeletonLoading';

const HistoryList = ({data, isLoading}) => {
  const navigation = useNavigation();

  const cardOnPress = (item) => {
    navigation.navigate('ViewAtDate', {data: item});
  };

  const renderItem = ({item}) => {
    return isLoading ? (
      <SkeletonLoading />
    ) : (
      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => cardOnPress(item)}
        style={styles.card}>
        <View style={styles.cardLeft}>
          <Icon
            type="MaterialCommunityIcons"
            name="calendar-range"
            style={styles.calendarIcon}
          />
          <View>
            <View style={styles.wrapName}>
              <Text style={styles.className}>{item.yogaClass.name}</Text>
              <Text style={styles.level}>
                {item.levelName ? ' - ' + item.levelName : ''}
              </Text>
            </View>
            <Text style={styles.time}>
              {moment(item.created_at).format('DD/MM/YYYY HH:mm')}
            </Text>
          </View>
        </View>
        <Icon
          type="MaterialCommunityIcons"
          name="chevron-right"
          style={styles.chevronRightIcon}
        />
      </TouchableOpacity>
    );
  };

  return (
    <FlatList
      data={isLoading ? new Array(5) : data}
      renderItem={renderItem}
      keyExtractor={(item, index) => index + ''}
      ListEmptyComponent={<EmptyList />}
      style={styles.flatlist}
      ItemSeparatorComponent={() => <ListDivider />}
    />
  );
};

export default HistoryList;
