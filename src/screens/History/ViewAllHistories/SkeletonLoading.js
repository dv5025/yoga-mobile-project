import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {View, StyleSheet} from 'react-native';

const SkeletonLoading = () => {
  return (
    <SkeletonPlaceholder>
      <View style={styles.container}>
        <View style={styles.image} />
        <View>
          <View style={[{width: 200, height: 15}, styles.text]} />
          <View style={[{width: 50, height: 10}, styles.text, styles.date]} />
        </View>
      </View>
    </SkeletonPlaceholder>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 24,
  },
  text: {
    borderRadius: 4,
  },
  date: {
    marginTop: 5,
  },
  image: {
    width: 30,
    height: 30,
    borderRadius: 4,
    marginRight: 16,
  },
});

export default SkeletonLoading;
