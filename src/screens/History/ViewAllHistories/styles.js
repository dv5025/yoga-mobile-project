import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: colors.lightgray,
  },
  flatlist: {
    flex: 1,
    paddingHorizontal: 10,
  },
  wrapHeader: {
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  card: {
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 20,
    paddingVertical: 20,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
  },
  calendarIcon: {
    marginRight: 10,
    color: colors.color_1,
  },
  cardLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  chevronRightIcon: {
    color: colors.color_1,
  },
  time: {
    fontSize: 12,
    color: '#818181',
  },
  wrapName: {
    flexDirection: 'row',
  },
  className: {
    fontWeight: 'bold',
    color: '#353535',
  },
  level: {
    color: '#595959',
  }
});

export default styles;
