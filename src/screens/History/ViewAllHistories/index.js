import React, {useState, useCallback} from 'react';
import {View, StatusBar, BackHandler, Alert} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useFocusEffect} from '@react-navigation/native';
import exitApp from 'src/helpers/exitApp';
import styles from './styles';
import appStyles from 'src/AppStyles';
import SimpleHeader from 'src/components/SimpleHeader';
import HistoryList from './HistoryList';
import {useSelector} from 'react-redux';
import historyApi from '../../../api/history';

export default () => {
  const {token} = useSelector((state) => state.auth);
  const [isLoading, setIsLoading] = useState(false);

  const [history, setHistory] = useState([]);

  useFocusEffect(
    useCallback(() => {
      getAllHistory();

      const backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        () => exitApp(BackHandler),
      );

      return () => backHandler.remove();
    }, []),
  );

  const getAllHistory = () => {
    setIsLoading(true);
    historyApi
      .getUserHistories(token)
      .then((res) => {
        setHistory(res.data);
      })
      .catch((err) => {
        Alert.alert(err);
      })
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      });
  };

  return (
    <SafeAreaView style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <View style={styles.wrapHeader}>
        <SimpleHeader title="History" />
      </View>
      <HistoryList data={history} isLoading={isLoading} />
    </SafeAreaView>
  );
};
