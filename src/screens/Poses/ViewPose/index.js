import React, {useState} from 'react';
import {View} from 'native-base';
import {StatusBar} from 'react-native';
import styles from './styles';
import appStyles, {colors} from 'src/AppStyles';
import AccentHeader from 'src/components/AccentHeader';
import PoseInfo from './PoseInfo';
import PoseVideo from './PoseVideo';
import {poseVdoUrl} from '../../../api/index';

export default ({route}) => {
  const {pose} = route.params;
  const [isShowInfo, setIsShowInfo] = useState(false);

  return (
    <View style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="light-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <AccentHeader
        title={pose.name}
        backgroundColor={colors.pink_violet}
        backBtn={true}
        rightIcon={isShowInfo ? 'information' : 'information-outline'}
        rightIconOnpress={() => setIsShowInfo(!isShowInfo)}
        rightIconStyle={styles.infoIcon}
      />
      <PoseVideo vdo={{uri: `${poseVdoUrl}/${pose.vdo.filename}`}} />
      <PoseInfo
        isVisible={isShowInfo}
        setIsShowInfo={setIsShowInfo}
        pose={pose}
      />
    </View>
  );
};
