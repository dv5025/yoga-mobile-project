import {StyleSheet, StatusBar} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: colors.lightgray,
  },
  infoIcon: {
    fontSize: 23,
  },
  scrollView: {
    flexGrow: 1,
    backgroundColor: '#FFFFFF',
    padding: 20,
    paddingTop: 0,
    alignItems: 'center',
  },
  modal: {
    justifyContent: 'flex-end',
    padding: 0,
    margin: 0,
    marginTop: StatusBar.currentHeight + 30,
  },
  hideBtnContainer: {
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  wrapHideBtn: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingVertical: 10,
  },
  hideBtn: {
    height: 5,
    width: 40,
    backgroundColor: '#C6C6C6',
    borderRadius: 10,
  },
  title: {
    marginVertical: 10,
    fontSize: 15,
    fontWeight: 'bold',
    color: '#000000',
  },
  poseImage: {
    height: 250,
    width: 250,
  },
  wrapDescription: {
    width: '100%',
    marginTop: 20,
  },
});

export default styles;
