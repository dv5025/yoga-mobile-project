import React from 'react';
import Video from 'react-native-video';
import appStyles from 'src/AppStyles';

const PoseVideo = ({vdo}) => {
  return (
    <Video
      source={vdo}
      ref={(ref) => {
        this.player = ref;
      }}
      onBuffer={this.onBuffer}
      onError={this.videoError}
      controls={true}
      resizeMode="cover"
      style={appStyles.fulfill}
      repeat={true}
    />
  );
};

export default PoseVideo;
