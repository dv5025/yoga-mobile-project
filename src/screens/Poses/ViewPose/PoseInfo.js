import React from 'react';
import Modal from 'react-native-modal';
import {ScrollView, Text, TouchableOpacity, View, Image} from 'react-native';
import styles from './styles';
import {poseImageUrl} from '../../../api/index';

const PoseInfo = ({isVisible, setIsShowInfo, pose}) => {
  const hideModal = () => {
    setIsShowInfo(false);
  };

  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={hideModal}
      onBackButtonPress={hideModal}
      statusBarTranslucent={true}
      propagateSwipe
      style={styles.modal}>
      <View style={styles.hideBtnContainer}>
        <TouchableOpacity style={styles.wrapHideBtn} onPress={hideModal}>
          <View style={styles.hideBtn} />
        </TouchableOpacity>
      </View>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <Text style={styles.title}>{pose.name}</Text>
        <Image
          source={{uri: `${poseImageUrl}/${pose.image.filename}`}}
          style={styles.poseImage}
        />
        <View style={styles.wrapDescription}>
          {pose.descriptions.map((item, index) => {
            return (
              <Text key={index}>
                {'\t\t\t' + (index + 1) + '. ' + item.description}
              </Text>
            );
          })}
        </View>
      </ScrollView>
    </Modal>
  );
};

export default PoseInfo;
