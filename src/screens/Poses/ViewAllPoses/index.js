import React, {useState, useCallback} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {StatusBar, View, BackHandler, Alert} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import styles from './styles';
import appStyles from 'src/AppStyles';
import PosesList from './PosesList';
import exitApp from 'src/helpers/exitApp';
import SearchButton from 'src/components/SearchButton';
import SimpleHeader from 'src/components/SimpleHeader';
import SearchBar from 'src/components/SearchBar';
import poseApi from '../../../api/pose';
import {useSelector} from 'react-redux';

export default () => {
  const {token} = useSelector((state) => state.auth);
  const [isLoading, setIsLoading] = useState(false);

  const [poses, setPoses] = useState([]);
  const [isShowSearchBar, setIsShowSearchBar] = useState(false);

  const [searchKeyword, setSearchKeyword] = useState('');

  useFocusEffect(
    useCallback(() => {
      getAllPoses();

      const backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        () => exitApp(BackHandler),
      );

      return () => backHandler.remove();
    }, []),
  );

  const getAllPoses = () => {
    setIsLoading(true);
    poseApi
      .getAllPoses(token)
      .then((res) => {
        setPoses(res.data);
      })
      .catch((err) => {
        Alert.alert(err);
      })
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      });
  };

  return (
    <SafeAreaView style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <View style={styles.wrapHeader}>
        <SimpleHeader
          title="Poses"
          description="Freedom to practice wherever and whenever you want."
          right={
            <SearchButton
              onPress={() => setIsShowSearchBar(!isShowSearchBar)}
            />
          }
        />
        <SearchBar
          placeholder="Search by pose name"
          onSubmitEditing={() => null}
          visible={isShowSearchBar}
          onChangeText={(text) => setSearchKeyword(text)}
        />
      </View>
      <PosesList
        data={
          searchKeyword
            ? poses.filter((item) => item.name.match(searchKeyword))
            : poses
        }
        isLoading={isLoading}
      />
    </SafeAreaView>
  );
};
