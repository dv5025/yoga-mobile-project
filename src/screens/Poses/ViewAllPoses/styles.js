import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: colors.lightgray,
  },
  flatlist: {
    flex: 1,
    paddingHorizontal: 8,
  },
  wrapHeader: {
    marginHorizontal: 8,
    marginBottom: 8,
  },
  card: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    paddingRight: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
  },
  wrapPoseImage: {
    width: 90,
    height: 90,
    borderColor: colors.color_2,
    borderWidth: 3,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 30,
  },
  poseImage: {
    width: 60,
    height: 60,
  },
  chevronRightIcon: {
    color: colors.color_2,
  },
  posename: {
    fontWeight: 'bold',
    fontSize: 13,
  },
});

export default styles;
