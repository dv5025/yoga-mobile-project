import React from 'react';
import {FlatList, Image, TouchableOpacity} from 'react-native';
import {Text, View, Icon} from 'native-base';
import EmptyList from 'src/components/EmptyList';
import styles from './styles';
import appStyles from 'src/AppStyles';
import {useNavigation} from '@react-navigation/native';
import Divider from 'src/components/ListDivider';
import {poseImageUrl} from '../../../api/index';
import SkeletonLoading from './SkeletonLoading';

const PosesList = ({data, isLoading}) => {
  const navigation = useNavigation();

  const cardOnPress = (pose) => {
    navigation.navigate('ViewPose', {pose});
  };

  const renderItem = ({item}) => {
    return isLoading ? (
      <SkeletonLoading />
    ) : (
      <TouchableOpacity
        style={styles.card}
        activeOpacity={0.6}
        onPress={() => cardOnPress(item)}>
        <View style={styles.wrapPoseImage}>
          <Image
            source={{uri: `${poseImageUrl}/${item.image.filename}`}}
            style={styles.poseImage}
          />
        </View>
        <View style={appStyles.fulfill}>
          <Text style={[appStyles.text, styles.posename]}>{item.name}</Text>
        </View>
        <Icon
          type="MaterialCommunityIcons"
          name="chevron-right"
          style={styles.chevronRightIcon}
        />
      </TouchableOpacity>
    );
  };

  return (
    <FlatList
      data={isLoading ? new Array(5) : data}
      renderItem={renderItem}
      keyExtractor={(item, index) => index + ''}
      ListEmptyComponent={
        <EmptyList
          label="No poses data"
          iconSource={require('src/assets/empty-poses.png')}
        />
      }
      style={styles.flatlist}
      ItemSeparatorComponent={() => <Divider />}
      ListFooterComponent={<Divider />}
    />
  );
};

export default PosesList;
