import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: colors.lightgray,
  },
  wrapHeader: {
    paddingTop: 10,
    paddingBottom: 15,
  },
  header: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#4D4D4D',
  },
  contentContainer: {
    backgroundColor: colors.lightgray,
    flex: 1,
    paddingBottom: 20,
  },
  form: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: 20,
    marginVertical: 10,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 8,
  },
  submitButton: {
    paddingHorizontal: 20,
    marginTop: 10,
  },
  button: {
    borderRadius: 100,
  },
});

export default styles;
