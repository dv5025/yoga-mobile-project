import React, {useState, useRef, Fragment} from 'react';
import {StatusBar, View, Text, Alert} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from './styles';
import appStyles from 'src/AppStyles';
import FormInput from '../../../components/FormInput';
import Button from 'src/components/Button';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useNavigation} from '@react-navigation/native';
import authApi from '../../../api/auth';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {isInputError, getErrorMessage} from '../../../helpers/validation';

export default () => {
  const navigation = useNavigation();
  const [isShowPassword, setIsShowPassword] = useState(false);
  const [isShowConfirmPassword, setIsShowConfirmPassword] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [inputTouched, setInputTouched] = useState({
    username: false,
    email: false,
    password: false,
    confirmPassword: false,
    firstname: false,
    lastname: false,
  });

  const emailInput = useRef();
  const passwrodInput = useRef();
  const confirmPasswordInput = useRef();
  const firstNameInput = useRef();
  const lastNameInput = useRef();

  const handleFormSubmit = (formValue) => {
    setIsLoading(true);
    const formdata = new FormData();
    formdata.append('username', formValue.username);
    formdata.append('email', formValue.email);
    formdata.append('password', formValue.password);
    formdata.append('firstname', formValue.firstname);
    formdata.append('lastname', formValue.lastname);
    authApi
      .register(formdata)
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
          Alert.alert('Registration successfully');
          navigation.goBack();
        }, 500);
      })
      .catch((err) => {
        setTimeout(() => {
          setIsLoading(false);
          Alert.alert(err);
        }, 500);
      });
  };

  const checkUsernameAvailable = (text) => {
    return authApi
      .checkUsernameAvailability(text)
      .then((res) => {
        if (res.data.available) {
          return true;
        } else {
          return false;
        }
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response);
        } else if (err.request) {
          Alert.alert(err.request._response);
        } else {
          console.log(err);
        }
      });
  };

  const checkEmailAvailable = (text) => {
    return authApi
      .checkEmailAvailability(text)
      .then((res) => {
        if (res.data.available) {
          return true;
        } else {
          return false;
        }
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response);
        } else if (err.request) {
          Alert.alert(err.request._response);
        } else {
          console.log(err);
        }
      });
  };

  const validationSchema = Yup.object().shape({
    username: Yup.string()
      .required('Please enter an username')
      .min(4, 'Username must have at least 4 characters ')
      .test('isUsernameAvailable', 'This username already taken', (value) =>
        checkUsernameAvailable(value),
      ),
    email: Yup.string()
      .required('Please enter an email')
      .email('Not in email format')
      .test('isUsernameAvailable', 'This email already taken', (value) =>
        checkEmailAvailable(value),
      ),
    password: Yup.string()
      .required('Please enter a password')
      .min(8, 'Password must have at least 8 characters '),
    confirmPassword: Yup.string()
      .oneOf([Yup.ref('password')], 'Password not match')
      .required('Confirm Password is required'),
    firstname: Yup.string()
      .required('Please enter first name')
      .min(2, 'Firstname must have at least 2 characters '),
    lastname: Yup.string()
      .required('Please enter last name')
      .min(2, 'Lastname must have at least 2 characters '),
  });

  return (
    <SafeAreaView style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <View style={[appStyles.center, styles.wrapHeader]}>
        <Text style={styles.header}>Registration</Text>
      </View>
      <KeyboardAwareScrollView contentContainerStyle={appStyles.scrollView}>
        <View style={styles.contentContainer}>
          <Formik
            initialValues={{
              username: '',
              email: '',
              password: '',
              confirmPassword: '',
              firstname: '',
              lastname: '',
            }}
            onSubmit={(values) => handleFormSubmit(values)}
            validationSchema={validationSchema}>
            {({handleChange, values, handleSubmit, errors, touched}) => (
              <Fragment>
                <View style={styles.form}>
                  <FormInput
                    label="Username"
                    placeholder="username"
                    labelIcon="account"
                    returnKeyType="next"
                    value={values.username}
                    onChangeText={(text) => {
                      setInputTouched({...inputTouched, username: true});
                      handleChange('username')(text);
                    }}
                    onSubmitEditing={() => emailInput.current.focus()}
                    inputError={isInputError(
                      touched.username,
                      errors.username,
                      inputTouched.username,
                    )}
                    errorMessage={getErrorMessage(
                      touched.username,
                      errors.username,
                      inputTouched.username,
                    )}
                  />
                  <FormInput
                    ref={emailInput}
                    label="Email"
                    labelIcon="email"
                    placeholder="example@email.com"
                    returnKeyType="next"
                    value={values.email}
                    onChangeText={(text) => {
                      setInputTouched({...inputTouched, email: true});
                      handleChange('email')(text);
                    }}
                    onSubmitEditing={() => passwrodInput.current.focus()}
                    inputError={isInputError(
                      touched.email,
                      errors.email,
                      inputTouched.email,
                    )}
                    errorMessage={getErrorMessage(
                      touched.email,
                      errors.email,
                      inputTouched.email,
                    )}
                  />
                </View>
                <View style={styles.form}>
                  <FormInput
                    ref={passwrodInput}
                    label="Password"
                    labelIcon="lock"
                    placeholder="password"
                    returnKeyType="next"
                    value={values.password}
                    onChangeText={(text) => {
                      setInputTouched({...inputTouched, password: true});
                      handleChange('password')(text);
                    }}
                    rightIcon={isShowPassword ? 'eye-off' : 'eye'}
                    rightIconOnPress={() => setIsShowPassword(!isShowPassword)}
                    secureTextEntry={!isShowPassword}
                    onSubmitEditing={() => confirmPasswordInput.current.focus()}
                    inputError={isInputError(
                      touched.password,
                      errors.password,
                      inputTouched.password,
                    )}
                    errorMessage={getErrorMessage(
                      touched.password,
                      errors.password,
                      inputTouched.password,
                    )}
                  />
                  <FormInput
                    ref={confirmPasswordInput}
                    label="Confirm password"
                    labelIcon="textbox-password"
                    placeholder="confirm password"
                    returnKeyType="next"
                    value={values.confirmPassword}
                    onChangeText={(text) => {
                      setInputTouched({...inputTouched, confirmPassword: true});
                      handleChange('confirmPassword')(text);
                    }}
                    rightIcon={isShowConfirmPassword ? 'eye-off' : 'eye'}
                    rightIconOnPress={() =>
                      setIsShowConfirmPassword(!isShowConfirmPassword)
                    }
                    secureTextEntry={!isShowConfirmPassword}
                    onSubmitEditing={() => firstNameInput.current.focus()}
                    inputError={isInputError(
                      touched.confirmPassword,
                      errors.confirmPassword,
                      inputTouched.confirmPassword,
                    )}
                    errorMessage={getErrorMessage(
                      touched.confirmPassword,
                      errors.confirmPassword,
                      inputTouched.confirmPassword,
                    )}
                  />
                </View>
                <View style={styles.form}>
                  <FormInput
                    ref={firstNameInput}
                    label="First name"
                    placeholder="firstname"
                    returnKeyType="next"
                    value={values.firstname}
                    onChangeText={(text) => {
                      setInputTouched({...inputTouched, firstname: true});
                      handleChange('firstname')(text);
                    }}
                    onSubmitEditing={() => lastNameInput.current.focus()}
                    inputError={isInputError(
                      touched.firstname,
                      errors.firstname,
                      inputTouched.firstname,
                    )}
                    errorMessage={getErrorMessage(
                      touched.firstname,
                      errors.firstname,
                      inputTouched.firstname,
                    )}
                  />
                  <FormInput
                    ref={lastNameInput}
                    label="Last name"
                    placeholder="lastname"
                    value={values.lastname}
                    onChangeText={(text) => {
                      setInputTouched({...inputTouched, lastname: true});
                      handleChange('lastname')(text);
                    }}
                    inputError={isInputError(
                      touched.lastname,
                      errors.lastname,
                      inputTouched.lastname,
                    )}
                    errorMessage={getErrorMessage(
                      touched.lastname,
                      errors.lastname,
                      inputTouched.lastname,
                    )}
                  />
                </View>
                <Button
                  loading={isLoading}
                  label="Submit"
                  containerStyle={styles.submitButton}
                  style={styles.button}
                  onPress={handleSubmit}
                />
              </Fragment>
            )}
          </Formik>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
