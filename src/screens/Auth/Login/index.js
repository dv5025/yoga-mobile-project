import React, {useState, useRef, Fragment} from 'react';
import {StatusBar, View, Image, TouchableOpacity, Alert} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from './styles';
import appStyles, {colors} from '../../../AppStyles';
import FormInput from 'src/components/FormInput';
import Button from 'src/components/Button';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Text, CheckBox} from 'native-base';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {login} from '../../../actions/AuthAction';
import authApi from '../../../api/auth';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {isInputError, getErrorMessage} from '../../../helpers/validation';

export default () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);

  const [inputTouched, setInputTouched] = useState({
    username: false,
    password: false,
  });
  const [isShowPassword, setIsShowPassword] = useState(true);
  const [rememberMe, setRememberMe] = useState(auth.remember);

  const [isLoading, setIsLoading] = useState(false);
  const [showWarning, setShowWarning] = useState(false);

  const passwordInput = useRef();

  const onPressLogin = (values) => {
    setIsLoading(true);
    authApi
      .login({
        emailOrUsername: values.username,
        password: values.password,
      })
      .then((res) => {
        setTimeout(() => {
          setIsLoading(false);
          if (rememberMe) {
            dispatch(
              login(res.data.token, values.username, values.password, true),
            );
          } else {
            dispatch(login(res.data.token, '', '', false));
          }
        }, 500);
      })
      .catch((err) => {
        setTimeout(() => {
          setIsLoading(false);
          if (err.response) {
            if (
              err.response.data?.message === 'invalid email or username' ||
              err.response.data?.message === 'invalid password'
            ) {
              setShowWarning(true);
            } else {
              console.log(err.response);
            }
          } else if (err.request) {
            Alert.alert(err.request._response);
          } else {
            console.log(err);
          }
        }, 500);
      });
  };

  const validationSchema = Yup.object().shape({
    username: Yup.string().required('Username is required'),
    password: Yup.string().required('Password is required'),
  });

  return (
    <SafeAreaView style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <KeyboardAwareScrollView
        contentContainerStyle={appStyles.scrollView}
        style={styles.contentContainer}>
        <View style={[appStyles.center, styles.wrapImage]}>
          <Image source={require('src/assets/logo.png')} style={styles.image} />
        </View>
        <Formik
          initialValues={{
            username: auth.username,
            password: auth.password,
          }}
          onSubmit={(values) => onPressLogin(values)}
          validationSchema={validationSchema}>
          {({handleChange, values, handleSubmit, errors, touched}) => (
            <Fragment>
              <View style={styles.form}>
                {showWarning ? (
                  <View style={appStyles.center}>
                    <Text style={styles.warningText}>
                      Username or password is invalid
                    </Text>
                  </View>
                ) : (
                  <></>
                )}
                <FormInput
                  placeholder="Username or email"
                  leftIcon="account-circle"
                  returnKeyType="next"
                  value={values.username}
                  onChangeText={(text) => {
                    setInputTouched({...inputTouched, username: true});
                    handleChange('username')(text);
                  }}
                  onSubmitEditing={() => passwordInput.current.focus()}
                  round
                  inputError={isInputError(
                    touched.username,
                    errors.username,
                    inputTouched.username,
                  )}
                  errorMessage={getErrorMessage(
                    touched.username,
                    errors.username,
                    inputTouched.username,
                  )}
                />
                <FormInput
                  ref={passwordInput}
                  placeholder="Password"
                  leftIcon="lock"
                  value={values.password}
                  onChangeText={(text) => {
                    setInputTouched({...inputTouched, password: true});
                    handleChange('password')(text);
                  }}
                  rightIcon={isShowPassword ? 'eye-off' : 'eye'}
                  rightIconOnPress={() => setIsShowPassword(!isShowPassword)}
                  secureTextEntry={isShowPassword}
                  round
                  inputError={isInputError(
                    touched.password,
                    errors.password,
                    inputTouched.password,
                  )}
                  errorMessage={getErrorMessage(
                    touched.password,
                    errors.password,
                    inputTouched.password,
                  )}
                />
                <View style={styles.wrapRememberMe}>
                  <CheckBox
                    checked={rememberMe}
                    color={colors.color_1}
                    onPress={() => setRememberMe(!rememberMe)}
                    style={styles.rememberCheckBox}
                  />
                  <Text style={styles.rememberMeText}>Remember me</Text>
                </View>
              </View>
              <View style={styles.wrapLoginButton}>
                <Button
                  color={colors.color_3}
                  loading={isLoading}
                  label="Log in"
                  style={styles.loginButton}
                  onPress={handleSubmit}
                />
              </View>
            </Fragment>
          )}
        </Formik>

        <View style={[appStyles.center, styles.wrapFoot]}>
          <Text style={styles.text}>Do you have as account ?</Text>
          <TouchableOpacity
            style={styles.wrapSignUpButton}
            onPress={() => navigation.navigate('Register')}>
            <Text style={styles.signUpText}>Sign up</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
