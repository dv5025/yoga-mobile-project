import {StyleSheet} from 'react-native';
import {colors} from '../../../AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: colors.pink,
  },
  contentContainer: {
    flex: 1,
  },
  wrapImage: {
    marginTop: 90,
    marginBottom: 50,
  },
  image: {
    width: 130,
    height: 130,
  },
  form: {
    marginTop: 15,
    marginBottom: 50,
    marginHorizontal: 40,
  },
  wrapLoginButton: {
    marginHorizontal: 40,
  },
  loginButton: {
    borderRadius: 100,
  },
  wrapFoot: {
    flexDirection: 'row',
    marginVertical: 20,
  },
  text: {
    color: '#5C5C5C',
    fontSize: 13,
  },
  wrapSignUpButton: {
    marginLeft: 5,
  },
  signUpText: {
    fontWeight: 'bold',
    color: '#000000',
  },
  wrapRememberMe: {
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  rememberCheckBox: {
    marginRight: 24,
  },
  rememberMeText: {
    fontSize: 13,
    color: colors.text_1,
  },
  warningText: {
    color: '#D13939',
  },
});

export default styles;
