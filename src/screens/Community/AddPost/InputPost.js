import React, {useState, Fragment} from 'react';
import {Icon} from 'native-base';
import {StyleSheet, TouchableOpacity, Image, View, Text} from 'react-native';
import {checkPermission as openImagePicker} from 'src/components/ImagePicker';
import FormInput from 'src/components/FormInput';
import Divider from 'src/components/ListDivider';
import appStyle from 'src/AppStyles';
import Button from 'src/components/Button';
import {colors} from 'src/AppStyles';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {isInputError, getErrorMessage} from '../../../helpers/validation';

const InputPost = ({image, setImage, onAddPress, isLoading}) => {
  const onImageSelected = (res) => {
    setImage(res);
  };

  const [inputTouched, setInputTouched] = useState({
    title: false,
    description: false,
  });

  const isDisableSaveButton = (errprMes, values) => {
    if (values.title === '' || values.description === '') {
      return true;
    } else {
      return errprMes.title || errprMes.description ? true : false;
    }
  };

  const validationSchema = Yup.object().shape({
    title: Yup.string().required('Title is required'),
    description: Yup.string().required('Description is required'),
  });

  return (
    <View style={styles.wrapContent}>
      <Formik
        initialValues={{
          title: '',
          description: '',
        }}
        onSubmit={(values) => onAddPress(values)}
        validationSchema={validationSchema}>
        {({handleChange, values, handleSubmit, errors, touched}) => (
          <Fragment>
            <Divider />

            <View style={styles.formItem}>
              <FormInput
                label="Title"
                placeholder="title"
                value={values.title}
                required
                onChangeText={(text) => {
                  setInputTouched({...inputTouched, title: true});
                  handleChange('title')(text);
                }}
                inputError={isInputError(
                  touched.title,
                  errors.title,
                  inputTouched.title,
                )}
                errorMessage={getErrorMessage(
                  touched.title,
                  errors.title,
                  inputTouched.title,
                )}
              />
            </View>

            <Divider />

            <View style={styles.formItem}>
              <FormInput
                label="Description"
                placeholder="description"
                required
                value={values.description}
                onChangeText={(text) => {
                  setInputTouched({...inputTouched, description: true});
                  handleChange('description')(text);
                }}
                multiline
                inputError={isInputError(
                  touched.description,
                  errors.description,
                  inputTouched.description,
                )}
                errorMessage={getErrorMessage(
                  touched.description,
                  errors.description,
                  inputTouched.description,
                )}
              />
            </View>

            <Divider />

            <View style={styles.formItem}>
              <View style={styles.wrapLabel}>
                <Text style={styles.label}>Image</Text>
                <Text style={styles.optional}> (optional)</Text>
              </View>
              <View style={appStyle.center}>
                <TouchableOpacity
                  style={styles.wrapImage}
                  onPress={() => openImagePicker(onImageSelected, 1000, 563)}>
                  {image !== '' ? (
                    <Image
                      source={{
                        uri: image.path,
                      }}
                      style={styles.image}
                      resizeMode="stretch"
                    />
                  ) : (
                    <></>
                  )}
                  <Icon
                    type="MaterialCommunityIcons"
                    name="camera"
                    style={styles.cameraIcon}
                  />
                </TouchableOpacity>
              </View>
            </View>

            <Divider />
            <View>
              <Button
                containerStyle={styles.addButtonPosition}
                colors={colors.pink_orange}
                gradient={true}
                label="Add"
                style={styles.addButton}
                onPress={handleSubmit}
                loading={isLoading}
                disabled={isDisableSaveButton(errors, values)}
              />
            </View>
          </Fragment>
        )}
      </Formik>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapContent: {
    marginHorizontal: 8,
  },
  formItem: {
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    padding: 8,
  },
  wrapLabel: {
    flexDirection: 'row',
    marginVertical: 8,
    alignItems: 'flex-end',
  },
  label: {
    color: '#3B3B3B',
    marginLeft: 5,
    fontSize: 15,
  },
  optional: {
    fontSize: 12,
  },
  wrapImage: {
    backgroundColor: '#E0E0E0',
    width: 368,
    height: 207,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cameraIcon: {
    position: 'absolute',
    color: '#4D4D4D',
    opacity: 0.6,
  },
  image: {
    width: 368,
    height: 207,
  },
  addButtonContainer: {
    width: 150,
  },
  addButton: {
    borderRadius: 100,
  },
});

export default InputPost;
