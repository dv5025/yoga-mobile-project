import React, {useState} from 'react';
import {StatusBar, View, Alert} from 'react-native';
import appStyles, {colors} from 'src/AppStyles';
import AccentHeader from 'src/components/AccentHeader';
import InputPost from './InputPost';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useNavigation} from '@react-navigation/native';
import postApi from '../../../api/post';
import {useSelector} from 'react-redux';

export default () => {
  const navigation = useNavigation();
  const {token} = useSelector((state) => state.auth);

  const [isLoading, setIsLoading] = useState(false);

  const [image, setImage] = useState('');

  const onAddPress = (values) => {
    setIsLoading(true);
    const formData = new FormData();
    formData.append('title', values.title);
    formData.append('description', values.description);
    if (image !== '') {
      formData.append('image', {
        uri: image.path,
        name: 'post.jpg',
        type: image.mime,
      });
    }
    postApi
      .addPost(token, formData)
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
          navigation.navigate('ViewPosts');
        }, 500);
      })
      .catch((err) => {
        Alert.alert(err);
      });
  };

  return (
    <View style={appStyles.fulfill}>
      <StatusBar
        barStyle="light-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <AccentHeader
        title="Add new post"
        backgroundColor={colors.orange_yellow}
        backBtn={true}
      />
      <View style={appStyles.fulfill}>
        <KeyboardAwareScrollView contentContainerStyle={appStyles.scrollView}>
          <View>
            <InputPost
              image={image}
              setImage={setImage}
              onAddPress={onAddPress}
              isLoading={isLoading}
            />
          </View>
        </KeyboardAwareScrollView>
      </View>
    </View>
  );
};
