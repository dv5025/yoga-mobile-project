import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  screenContainer: {
    backgroundColor: colors.lightgray,
  },
  wrapSelector: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 8,
  },
});

export default styles;
