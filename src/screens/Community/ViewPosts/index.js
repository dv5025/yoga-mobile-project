import React, {useCallback} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {StatusBar, View, BackHandler} from 'react-native';
import exitApp from 'src/helpers/exitApp';
import appStyles, {colors} from 'src/AppStyles';
import AccentHeader from 'src/components/AccentHeader';
import ShowPosts from './ShowPosts';

export default () => {
  useFocusEffect(
    useCallback(() => {
      const backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        () => exitApp(BackHandler),
      );

      return () => backHandler.remove();
    }, []),
  );

  return (
    <View style={appStyles.fulfill}>
      <StatusBar
        barStyle="light-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <AccentHeader title="Community" backgroundColor={colors.orange_yellow} />
      <ShowPosts />
    </View>
  );
};
