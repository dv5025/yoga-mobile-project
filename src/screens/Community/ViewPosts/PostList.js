import React from 'react';
import {FlatList, Alert} from 'react-native';
import CardImage from 'src/components/CardImage';
import EmptyList from 'src/components/EmptyList';
import {useNavigation} from '@react-navigation/native';
import {profileImageUrl, postImageUrl} from '../../../api/index';
import postApi from '../../../api/post';
import {useSelector} from 'react-redux';
import SkeletonLoading from '../SkeletonLoading';

const PostList = ({
  data,
  showOptionButton,
  onDeleteDone,
  isLoading,
  setIsLoading,
  onRefresh,
}) => {
  const navigation = useNavigation();
  const {token} = useSelector((state) => state.auth);

  const goToViewComment = (id) => {
    navigation.navigate('ViewComment', {postId: id});
  };

  const goToEditPost = (id) => {
    navigation.navigate('EditPost', {postId: id});
  };

  const onDeletePress = (id) => {
    postApi
      .deletePostById(token, id)
      .then(() => onDeleteDone())
      .catch((err) => Alert.alert(err));
  };

  const renderItem = ({item}) => {
    return isLoading ? (
      <SkeletonLoading />
    ) : (
      <CardImage
        userImage={
          item.user.profileImage.filename
            ? {uri: `${profileImageUrl}/${item.user.profileImage.filename}`}
            : ''
        }
        title={item.title}
        createDate={item.created_at}
        image={
          item.image.filename
            ? {uri: `${postImageUrl}/${item.image.filename}`}
            : null
        }
        description={item.description}
        numberOfLinesDescription={4}
        numComment={item.comments.length}
        creator={item.user.username}
        showOptionButton={showOptionButton}
        cardOnPress={() => goToViewComment(item.id)}
        commentOnpress={() => goToViewComment(item.id)}
        onEditPressed={() => goToEditPost(item.id)}
        onDeletePressed={() => onDeletePress(item.id)}
      />
    );
  };

  const onRefreshPost = () => {
    setIsLoading(true);
    onRefresh();
  };

  return (
    <FlatList
      data={isLoading ? new Array(3) : data}
      renderItem={renderItem}
      keyExtractor={(item, index) => index + ''}
      ListEmptyComponent={<EmptyList />}
      onRefresh={onRefreshPost}
      refreshing={isLoading}
    />
  );
};

export default PostList;
