import React, {useEffect, useState} from 'react';
import {View, Alert} from 'react-native';
import appStyles from 'src/AppStyles';
import AddBtn from 'src/components/AddBtn';
import PostList from './PostList';
import {useNavigation} from '@react-navigation/native';
import Segment, {SegmentItem} from 'src/components/Segment';
import styles from './styles';
import postApi from '../../../api/post';
import {useSelector} from 'react-redux';

const ShowPosts = () => {
  const navigation = useNavigation();
  const {token} = useSelector((state) => state.auth);
  const [isLoading, setIsLoading] = useState(false);

  const [data, setData] = useState([]);
  const [currentShow, setCurrentShow] = useState('ALL');

  useEffect(() => {
    fetchData();
  }, [currentShow]);

  const fetchData = () => {
    setIsLoading(true);
    if (currentShow === 'ALL') {
      postApi
        .getAllPost(token)
        .then((res) => {
          setData(res.data);
        })
        .catch((err) => {
          Alert.alert(err);
        })
        .then(() => {
          setTimeout(() => {
            setIsLoading(false);
          }, 500);
        });
    } else if (currentShow === 'MY_POST') {
      postApi
        .getUserPosts(token)
        .then((res) => {
          setData(res.data);
        })
        .catch((err) => {
          Alert.alert(err);
        })
        .then(() => {
          setTimeout(() => {
            setIsLoading(false);
          }, 500);
        });
    } else {
      setData([]);
    }
  };

  return (
    <View style={[appStyles.fulfill]}>
      <View style={styles.wrapSelector}>
        <Segment
          initialValue="ALL"
          onSelected={(value) => setCurrentShow(value)}>
          <SegmentItem value="ALL">All</SegmentItem>
          <SegmentItem value="MY_POST">My Post</SegmentItem>
        </Segment>
      </View>
      <PostList
        data={data}
        showOptionButton={currentShow === 'MY_POST' ? true : false}
        onDeleteDone={fetchData}
        isLoading={isLoading}
        setIsLoading={setIsLoading}
        onRefresh={fetchData}
      />
      <AddBtn onPress={() => navigation.navigate('AddPost')} />
    </View>
  );
};

export default ShowPosts;
