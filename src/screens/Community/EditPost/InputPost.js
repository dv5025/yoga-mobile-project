import React from 'react';
import {Card, CardItem, Text, Body, Right, Left, Icon} from 'native-base';
import {StyleSheet, TextInput, TouchableOpacity, Image} from 'react-native';
import {checkPermission as openImagePicker} from 'src/components/ImagePicker';
import {postImageUrl} from '../../../api/index';

const InputPost = ({
  creator,
  title,
  setTitle,
  image,
  setImage,
  description,
  setDescription,
}) => {
  const onImageSelected = (res) => {
    setImage(res);
  };

  return (
    <Card>
      <CardItem>
        <Body>
          <TextInput
            placeholder="Press to add title"
            style={styles.title}
            value={title}
            onChangeText={(text) => setTitle(text)}
          />
        </Body>
      </CardItem>
      <CardItem>
        <Body>
          <TouchableOpacity
            style={styles.wrapImage}
            onPress={() => openImagePicker(onImageSelected)}>
            {image.filename !== '' ? (
              <Image
                source={{uri: `${postImageUrl}/${image.filename}`}}
                style={styles.image}
                resizeMode="stretch"
              />
            ) : image ? (
              <Image
                source={{uri: image.path}}
                style={styles.image}
                resizeMode="stretch"
              />
            ) : (
              <></>
            )}
            {/* {image !== '' || image !== undefined ? (
              <Image source={image} style={styles.image} resizeMode="stretch" />
            ) : (
              <></>
            )} */}
            <Icon
              type="MaterialCommunityIcons"
              name="camera"
              style={styles.cameraIcon}
            />
          </TouchableOpacity>
          <TextInput
            placeholder="Press to add description . . ."
            multiline={true}
            value={description}
            onChangeText={(text) => setDescription(text)}
          />
        </Body>
      </CardItem>
      <CardItem>
        <Left />
        <Right>
          <Text style={styles.createBy}>{'created by @' + creator}</Text>
        </Right>
      </CardItem>
    </Card>
  );
};

const styles = StyleSheet.create({
  createBy: {
    fontSize: 13,
    color: '#838383',
  },
  title: {
    width: '100%',
    borderBottomWidth: 0.5,
    borderColor: '#CBCBCB',
    fontSize: 18,
    fontWeight: 'bold',
  },
  wrapImage: {
    backgroundColor: '#E0E0E0',
    width: '100%',
    height: 200,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cameraIcon: {
    position: 'absolute',
    color: '#4D4D4D',
    opacity: 0.6,
  },
  image: {
    width: '100%',
    height: 200,
    borderRadius: 8,
  },
});

export default InputPost;
