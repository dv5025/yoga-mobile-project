import React, {useState, useCallback} from 'react';
import {StatusBar, View, StyleSheet} from 'react-native';
import appStyles, {colors} from 'src/AppStyles';
import AccentHeader from 'src/components/AccentHeader';
import InputPost from './InputPost';
import Button from 'src/components/Button';
import {useNavigation} from '@react-navigation/native';
import {useFocusEffect} from '@react-navigation/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import postApi from '../../../api/post';
import {useSelector} from 'react-redux';

export default ({route}) => {
  const navigation = useNavigation();
  const {token} = useSelector((state) => state.auth);
  const {postId} = route.params;

  const [title, setTitle] = useState('');
  const [image, setImage] = useState('');
  const [description, setDescription] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  useFocusEffect(
    useCallback(() => {
      fetchData();
    }, []),
  );

  const fetchData = () => {
    postApi
      .getPostById(token, postId)
      .then((res) => {
        setTitle(res.data.title);
        setImage(res.data.image);
        setDescription(res.data.description);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onSavePress = () => {
    setIsLoading(true);
    const formData = new FormData();
    formData.append('title', title);
    formData.append('description', description);
    if (image.filename !== '') {
      formData.append('image', {
        uri: image.path,
        name: 'post.jpg',
        type: image.mime,
      });
    }
    postApi
      .editPost(token, formData)
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
          navigation.navigate('ViewPosts');
        }, 500);
      })
      .catch((err) => {
        setIsLoading(false);
        console.log(err);
      });
  };

  return (
    <View style={appStyles.fulfill}>
      <StatusBar
        barStyle="light-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <AccentHeader
        title="Edit post"
        backgroundColor={colors.orange_yellow}
        backBtn={true}
      />
      <View style={appStyles.fulfill}>
        <KeyboardAwareScrollView contentContainerStyle={appStyles.scrollView}>
          <View style={styles.inputContainer}>
            <InputPost
              creator="sdas"
              title={title}
              setTitle={setTitle}
              image={image}
              setImage={setImage}
              description={description}
              setDescription={setDescription}
            />
          </View>
        </KeyboardAwareScrollView>
      </View>
      <View style={styles.center}>
        <Button
          containerStyle={styles.addButtonContainer}
          gradient={true}
          label="Save"
          style={styles.addButton}
          onPress={onSavePress}
          loading={isLoading}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
    width: '100%',
    position: 'absolute',
    bottom: 10,
  },
  addButtonContainer: {
    width: 150,
  },
  addButton: {
    borderRadius: 100,
  },
  inputContainer: {
    paddingBottom: 70,
  },
});
