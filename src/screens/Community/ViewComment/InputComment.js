import React, {useState} from 'react';
import {TextInput, TouchableOpacity, View, Alert} from 'react-native';
import {Card, CardItem, Thumbnail, Text, Left, Body, Icon} from 'native-base';
import styles from './styles';
import postApi from '../../../api/post';
import {useSelector} from 'react-redux';

const InputComment = ({userImage, creator, onAddDone, postId}) => {
  const [message, setMessage] = useState('');
  const {token} = useSelector((state) => state.auth);

  const onSend = () => {
    postApi
      .addComment(token, message, postId)
      .then(() => {
        setMessage('');
      })
      .catch((err) => {
        Alert.alert(err);
      })
      .then(() => {
        onAddDone();
      });
  };

  const verifyCommentInput = () => {
    if (message.length === 0) {
      return {valid: false};
    } else {
      return {valid: true};
    }
  };

  return (
    <Card>
      <CardItem>
        <Left>
          <Thumbnail
            source={
              userImage && userImage != null
                ? userImage
                : require('src/assets/user.png')
            }
          />
          <Body>
            <Text>{creator}</Text>
          </Body>
        </Left>
      </CardItem>
      <CardItem>
        <Body>
          <TextInput
            style={styles.textInput}
            value={message}
            onChangeText={(text) => setMessage(text)}
            placeholder="What's on your mind?"
          />
          <View style={styles.wrapSendButton}>
            <TouchableOpacity
              activeOpacity={verifyCommentInput().valid ? 0.5 : 1}
              onPress={verifyCommentInput().valid ? onSend : () => null}>
              <Icon
                type="MaterialCommunityIcons"
                name="send"
                style={
                  verifyCommentInput().valid
                    ? styles.sendIcon
                    : styles.sendIconDisable
                }
              />
            </TouchableOpacity>
          </View>
        </Body>
      </CardItem>
    </Card>
  );
};

export default InputComment;
