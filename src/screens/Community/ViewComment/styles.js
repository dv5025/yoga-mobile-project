import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  listHeaderContainer: {
    marginBottom: 10,
  },
  card: {
    marginBottom: 20,
  },
  wrapCommentTitle: {
    marginHorizontal: 5,
  },
  wrapComment: {
    marginHorizontal: 10,
  },
  wrapSendButton: {
    alignItems: 'flex-end',
    width: '100%',
  },
  sendIcon: {
    color: '#1764FF',
  },
  sendIconDisable: {
    color: '#8B8B8B',
  },
  textInput: {
    width: '100%',
  },
  commentHeader: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000000',
  },
});

export default styles;
