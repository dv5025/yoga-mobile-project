import React from 'react';
import CardImage from 'src/components/CardImage';
import {View, Text} from 'react-native';
import styles from './styles';
import InputComment from './InputComment';
import {profileImageUrl, postImageUrl} from '../../../api/index';

const ListHeader = ({data, getPostData}) => {
  return (
    <View style={styles.listHeaderContainer}>
      <CardImage
        userImage={
          data.user?.profileImage.filename
            ? {uri: `${profileImageUrl}/${data.user.profileImage.filename}`}
            : ''
        }
        title={data.title}
        createDate={data.created_at}
        image={
          data.image?.filename
            ? {uri: `${postImageUrl}/${data.image.filename}`}
            : null
        }
        description={data.description}
        numComment={data.comments?.length}
        creator={data.user?.username}
        style={styles.card}
      />
      <View style={styles.wrapCommentTitle}>
        <Text style={styles.commentHeader}>Comments</Text>
        <InputComment
          userImage={
            data.user?.profileImage.filename
              ? {uri: `${profileImageUrl}/${data.user.profileImage.filename}`}
              : ''
          }
          creator={data.user?.username}
          onAddDone={getPostData}
          postId={data.id}
        />
      </View>
    </View>
  );
};

export default ListHeader;
