import React, {useCallback, useState} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {StatusBar, View, Alert} from 'react-native';
import appStyles, {colors} from 'src/AppStyles';
import AccentHeader from 'src/components/AccentHeader';
import Content from './Content';
import postApi from '../../../api/post';
import {useSelector} from 'react-redux';

export default ({route}) => {
  const {postId} = route.params;
  const {token} = useSelector((state) => state.auth);
  const [isLoading, setIsLoading] = useState(false);

  const [data, setData] = useState([]);

  useFocusEffect(
    useCallback(() => {
      getPostData();
    }, []),
  );

  const getPostData = () => {
    setIsLoading(true);
    postApi
      .getPostById(token, postId)
      .then((res) => {
        setData(res.data);
      })
      .catch((err) => {
        Alert.alert(err);
      })
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      });
  };

  return (
    <View style={appStyles.fulfill}>
      <StatusBar
        barStyle="light-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <AccentHeader
        title="Community"
        backgroundColor={colors.orange_yellow}
        backBtn={true}
      />
      <Content data={data} getPostData={getPostData} isLoading={isLoading} />
    </View>
  );
};
