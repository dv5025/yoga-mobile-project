import React from 'react';
import {Card, CardItem, Thumbnail, Text, Left, Body} from 'native-base';

const CardComment = ({userImage, creator, createDate, message}) => {
  return (
    <Card>
      <CardItem>
        <Left>
          <Thumbnail
            source={
              userImage && userImage != null
                ? userImage
                : require('src/assets/user.png')
            }
          />
          <Body>
            <Text>{creator}</Text>
            <Text note>{createDate}</Text>
          </Body>
        </Left>
      </CardItem>
      <CardItem>
        <Body>
          <Text>{message}</Text>
        </Body>
      </CardItem>
    </Card>
  );
};

export default CardComment;
