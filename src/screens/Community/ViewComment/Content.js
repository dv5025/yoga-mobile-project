import React from 'react';
import {FlatList, View} from 'react-native';
import Empty from 'src/components/EmptyList';
import ListHeader from './ListHeader';
import CardComment from './CardComment';
import moment from 'moment';
import {profileImageUrl} from '../../../api/index';
import SkeletonLoading from '../SkeletonLoading';

import styles from './styles';

const Content = ({data, getPostData, isLoading}) => {
  const renderItem = ({item}) => {
    return (
      <View style={styles.wrapComment}>
        <CardComment
          userImage={
            item.user.profileImage.filename
              ? {uri: `${profileImageUrl}/${item.user.profileImage.filename}`}
              : ''
          }
          creator={item.user.username}
          createDate={moment(item.created_at).format('MMM D, YYYY HH:mm')}
          message={item.message}
        />
      </View>
    );
  };

  return isLoading ? (
    <SkeletonLoading />
  ) : (
    <FlatList
      data={data.comments}
      renderItem={renderItem}
      keyExtractor={(item, index) => index + ''}
      ListEmptyComponent={<Empty />}
      ListHeaderComponent={<ListHeader data={data} getPostData={getPostData} />}
    />
  );
};

export default Content;
