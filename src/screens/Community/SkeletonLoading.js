import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {View, StyleSheet} from 'react-native';

const SkeletonLoading = () => {
  return (
    <SkeletonPlaceholder>
      <View style={styles.container}>
        <View style={styles.wrapHeader}>
          <View style={styles.profieImage} />
          <View>
            <View style={[{width: 200, height: 20}, styles.text]} />
            <View style={[{width: 80}, styles.text, styles.description]} />
          </View>
        </View>
        <View>
          <View style={styles.image} />
          <View style={[{width: '100%'}, styles.text, styles.description]} />
          <View style={[{width: '90%'}, styles.text, styles.description]} />
          <View style={[{width: '70%'}, styles.text, styles.description]} />
        </View>
      </View>
    </SkeletonPlaceholder>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    borderRadius: 16,
    borderColor: '#ECECEC',
    borderWidth: 0.5,
    padding: 16,
    marginHorizontal: 8,
    marginVertical: 1,
  },
  wrapHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    borderRadius: 4,
  },
  description: {
    height: 10,
    marginTop: 6,
  },
  profieImage: {
    width: 55,
    height: 55,
    borderRadius: 50,
    marginRight: 16,
  },
  image: {
    height: 150,
    borderRadius: 8,
    marginTop: 16,
    marginBottom: 8,
  },
});

export default SkeletonLoading;
