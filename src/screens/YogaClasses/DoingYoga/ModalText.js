import React from 'react';
import Modal from 'react-native-modal';
import {View} from 'react-native';
import {Text} from 'native-base';
import {useNavigation} from '@react-navigation/native';
import appStyles from 'src/AppStyles';
import styles from './styles';

const ModalPoseName = ({isVisible, label}) => {
  const navigation = useNavigation();

  const goBack = () => {
    navigation.goBack();
  };

  return (
    <Modal
      isVisible={isVisible}
      statusBarTranslucent={true}
      onBackButtonPress={goBack}>
      <View style={[appStyles.center, styles.modalContainer]}>
        <Text style={styles.poseNameModal}>{label}</Text>
      </View>
    </Modal>
  );
};

export default ModalPoseName;
