import React from 'react';
import {View, Text} from 'react-native';
import ProgressBarComponent from 'src/components/ProgressBar';
import styles from './styles';

const ProgressBar = ({currentPose, data}) => {
  const calProgress = () => {
    return currentPose === -1 ? 0 : (currentPose + 1) / data.length;
  };

  return (
    <View style={styles.wrapProgressBar}>
      <ProgressBarComponent progress={calProgress()} width={350} />
      <Text style={styles.progressDes}>
        {currentPose + 1 + ' of ' + data.length}
      </Text>
    </View>
  );
};

export default ProgressBar;
