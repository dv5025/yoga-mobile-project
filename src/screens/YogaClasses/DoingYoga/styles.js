import {StyleSheet, Dimensions} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  screenContainer: {
    backgroundColor: colors.lightgray,
  },
  scrollView: {
    flexGrow: 1,
  },
  modalContainer: {
    flex: 1,
  },
  poseNameContainer: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    margin: 16,
    padding: 16,
    borderRadius: 8,
    alignItems: 'center',
    minHeight: 110,
  },
  poseName: {
    fontWeight: 'bold',
    flex: 1,
    paddingRight: 10,
  },
  poseNameModal: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 25,
    textAlign: 'center',
    width: 300,
  },
  vdoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: '#FFFFFF',
    marginHorizontal: 16,
    borderRadius: 8,
  },
  vdo: {
    height: 400,
    width: Math.round(Dimensions.get('window').width),
  },
  poseImage: {
    height: 80,
    width: 80,
    borderRadius: 50,
    borderColor: colors.color_1,
    borderWidth: 2,
  },
  wrapProgressBar: {
    alignItems: 'center',
    marginHorizontal: 16,
    marginTop: 30,
    marginBottom: 20,
  },
  progressDes: {
    marginTop: 5,
  },
});

export default styles;
