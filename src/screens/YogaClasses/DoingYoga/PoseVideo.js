import React from 'react';
import Video from 'react-native-video';
import styles from './styles';

const PoseVideo = ({vdo, onEnd, paused}) => {
  return (
    <Video
      source={vdo}
      ref={(ref) => {
        this.player = ref;
      }}
      onBuffer={this.onBuffer}
      onError={this.videoError}
      resizeMode="contain"
      style={styles.vdo}
      onEnd={onEnd}
      paused={paused}
    />
  );
};

export default PoseVideo;
