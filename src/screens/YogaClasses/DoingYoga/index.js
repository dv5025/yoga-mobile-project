import React, {useCallback, useState} from 'react';
import {View, StatusBar, Image, ScrollView, Alert} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import AccentHeader from 'src/components/AccentHeader';
import appStyles from 'src/AppStyles';
import styles from './styles';
import ModalText from './ModalText';
import {Text} from 'native-base';
import PoseVideo from './PoseVideo';
import ProgressBar from './ProgressBar';
import Timer from 'src/components/Timer';
import {useNavigation} from '@react-navigation/native';
import LoadingScreen from 'src/components/LoadingScreen';
import yogaClassApi from '../../../api/yogaClass';
import {useSelector} from 'react-redux';
import {poseImageUrl, poseVdoUrl} from '../../../api/index';
import Tts from 'react-native-tts';
import AudioPlayer from '../../../components/AudioPlayer';
import historyApi from '../../../api/history';

export default ({route}) => {
  Tts.setDucking(true);
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);
  const {levelId, customClassId} = route.params;
  const [classId, setClassId] = useState();
  const {token} = useSelector((state) => state.auth);

  const [data, setData] = useState([]);
  const [currentPose, setCurrentPose] = useState(-1);

  const [vdoPaused, setVdoPaused] = useState(true);
  const [isShowModalPoseName, setIsShowModalPoseName] = useState(false);
  const [isShowModalTime, setIsShowModalTime] = useState(false);
  const [isPreview, setIsPreview] = useState(true);
  const [isDoing, setIsDoing] = useState(false);

  useFocusEffect(
    useCallback(() => {
      if (currentPose === -1) {
        AudioPlayer.playBackgroundSound();
        getData();
      }
      if (currentPose <= data.length - 1) {
        showModalPoseName();
      }
    }, [currentPose]),
  );

  const getData = () => {
    setIsLoading(true);
    if (levelId) {
      yogaClassApi
        .getYogaLevelById(token, levelId)
        .then((res) => {
          setClassId(res.data.yogaClass.id);
          setData(
            res.data.poses.map((item) => {
              return {
                name: item.poses.name,
                vdo: item.poses.vdo,
                time: item.time,
                image: item.poses.image,
              };
            }),
          );
          setCurrentPose(0);
        })
        .catch((err) => {
          Alert.alert(err);
        })
        .then(() => {
          setIsLoading(false);
        });
    } else {
      yogaClassApi
        .getCustomClassById(token, customClassId)
        .then((res) => {
          setData(
            res.data.poses.map((item) => {
              return {
                name: item.poses.name,
                vdo: item.poses.vdo,
                time: item.time,
                image: item.poses.image,
              };
            }),
          );
          setCurrentPose(0);
        })
        .catch((err) => {
          Alert.alert(err);
        })
        .then(() => {
          setIsLoading(false);
        });
    }
  };

  const showModalPoseName = () => {
    Tts.stop();
    if (data[currentPose]) {
      if (currentPose >= 1) {
        Tts.speak('next');
      }
      Tts.speak(data[currentPose].name);
    }
    setIsShowModalPoseName(true);
    setVdoPaused(true);
    setTimeout(() => {
      setIsShowModalPoseName(false);
      setVdoPaused(false);
    }, 3000);
  };

  const onVdoEnd = () => {
    Tts.stop();
    Tts.speak(data[currentPose].time + ' seconds');
    setIsShowModalTime(true);
    setTimeout(() => {
      setIsShowModalTime(false);
      startDoing();
    }, 3000);
  };

  const startDoing = () => {
    Tts.stop();
    Tts.speak('start');
    setIsPreview(false);
    setIsDoing(true);
  };

  const onDoingDone = () => {
    setTimeout(() => {
      setIsPreview(true);
      setIsDoing(false);
      nextPose();
    }, 1500);
  };

  const nextPose = () => {
    if (currentPose < data.length - 1) {
      setCurrentPose(currentPose + 1);
      showModalPoseName();
    } else {
      setIsDoing(false);
      Tts.stop();
      Tts.speak('congratulations');
      Alert.alert('Done');
      AudioPlayer.stop();
      if (customClassId) {
        historyApi
          .addHistory(token, customClassId, 0)
          .then(() => null)
          .catch((err) => console.log(err))
          .then(() => navigation.goBack());
      } else {
        historyApi
          .addHistory(token, classId, levelId)
          .then(() => null)
          .catch((err) => console.log(err))
          .then(() => navigation.goBack());
      }
    }
  };

  const getTitle = () => {
    if (isPreview) {
      return 'Preview';
    } else if (isDoing) {
      return 'Doing';
    } else {
      return 'Done';
    }
  };

  return (
    <View style={[appStyles.fulfill, styles.screenContainer]}>
      <StatusBar
        barStyle="light-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <AccentHeader title={getTitle()} backBtn={true} />
      <ScrollView contentContainerStyle={styles.scrollView}>
        <View style={styles.poseNameContainer}>
          <Text style={styles.poseName}>{data[currentPose]?.name}</Text>
          {isDoing ? (
            <Image
              source={{
                uri: `${poseImageUrl}/${data[currentPose]?.image.filename}`,
              }}
              style={styles.poseImage}
            />
          ) : (
            <></>
          )}
        </View>
        <View style={styles.vdoContainer}>
          {isPreview ? (
            <PoseVideo
              vdo={
                data[currentPose]
                  ? {uri: `${poseVdoUrl}/${data[currentPose].vdo.filename}`}
                  : null
              }
              onEnd={onVdoEnd}
              paused={vdoPaused}
            />
          ) : (
            <Timer
              size={300}
              time={data[currentPose]?.time}
              onTimeUp={onDoingDone}
              thickness={8}
            />
          )}
        </View>
        <ProgressBar currentPose={currentPose} data={data} />
      </ScrollView>
      <ModalText
        label={data[currentPose]?.name}
        isVisible={isShowModalPoseName}
      />
      <ModalText
        label={data[currentPose]?.time + ' seconds'}
        isVisible={isShowModalTime}
      />
      <LoadingScreen visible={isLoading} />
    </View>
  );
};
