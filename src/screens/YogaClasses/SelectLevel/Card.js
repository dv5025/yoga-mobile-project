import React from 'react';
import {View, Text, Image, TouchableOpacity, Alert} from 'react-native';
import styles from './styles';
import Button from 'src/components/Button';
import {useNavigation} from '@react-navigation/native';

const Card = ({data}) => {
  const navigation = useNavigation();
  const {id, levelName, title, description} = data;

  const showPosesOnPress = () => {
    navigation.navigate('ShowPoses', {levelId: id});
  };

  const startLevel = () => {
    Alert.alert(
      'Are you ready ?',
      '',
      [
        {
          text: 'NO',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'YES',
          onPress: () => navigation.navigate('DoingYoga', {levelId: id}),
        },
      ],
      {cancelable: false},
    );
  };

  return (
    <View style={styles.card}>
      <Text style={styles.levelName}>{levelName}</Text>
      <Image source={getImage(levelName)} style={styles.cardImage} />
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.description}>{description}</Text>
      <Button
        label="Start"
        onPress={startLevel}
        containerStyle={styles.button}
      />
      <TouchableOpacity onPress={showPosesOnPress}>
        <Text style={styles.showPoses}>Show Poses</Text>
      </TouchableOpacity>
    </View>
  );
};

const getImage = (level) => {
  if (level === 'Beginner') {
    return require('src/assets/beginner.png');
  } else if (level === 'Intermediate') {
    return require('src/assets/intermediate.png');
  } else {
    return require('src/assets/advance.png');
  }
};

export default Card;
