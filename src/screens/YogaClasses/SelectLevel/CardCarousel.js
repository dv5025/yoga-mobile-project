import React from 'react';
import Carousel from 'react-native-snap-carousel';
import Card from './Card';
import {Dimensions} from 'react-native';

const CardCarousel = ({data}) => {
  const renderItem = ({item}) => {
    return <Card data={item} />;
  };

  return (
    <Carousel
      data={data ? data : []}
      renderItem={renderItem}
      sliderWidth={Math.round(Dimensions.get('window').width)}
      itemWidth={330}
    />
  );
};

export default CardCarousel;
