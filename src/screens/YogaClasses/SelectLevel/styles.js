import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  screenContainer: {
    backgroundColor: colors.lightgray,
  },
  instruction: {
    textAlign: 'center',
    marginTop: 70,
    marginBottom: 15,
  },
  card: {
    backgroundColor: '#FFFFFF',
    width: 330,
    borderRadius: 8,
    padding: 15,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  levelName: {
    fontWeight: 'bold',
    color: colors.color_2,
    fontSize: 18,
    marginTop: 15,
  },
  cardImage: {
    width: 100,
    height: 100,
    marginVertical: 30,
  },
  title: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 12,
  },
  description: {
    fontSize: 12,
    textAlign: 'center',
    width: 250,
    marginVertical: 10,
  },
  showPoses: {
    fontWeight: 'bold',
    fontSize: 12,
    color: '#B2B2B2',
  },
  button: {
    marginTop: 15,
    marginBottom: 20,
    width: 230,
  },
});

export default styles;
