import React, {useState, useCallback} from 'react';
import {View} from 'native-base';
import {StatusBar, Text, Alert} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import styles from './styles';
import appStyles from 'src/AppStyles';
import AccentHeader from 'src/components/AccentHeader';
import CardCarousel from './CardCarousel';
import yogaClassApi from '../../../api/yogaClass';
import {useSelector} from 'react-redux';
import AudioPlayer from '../../../components/AudioPlayer';

export default ({route}) => {
  const {yogaClassId} = route.params;
  const {token} = useSelector((state) => state.auth);
  const [isLoading, setIsLoading] = useState(false);

  const [yogaClasse, setYogaClasse] = useState({});

  useFocusEffect(
    useCallback(() => {
      getAllYogaClasses();
      AudioPlayer.stop();
    }, []),
  );

  const getAllYogaClasses = () => {
    setIsLoading(true);
    yogaClassApi
      .getPresetClassById(token, yogaClassId)
      .then((res) => {
        setYogaClasse(res.data);
      })
      .catch((err) => {
        Alert.alert(err);
      })
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      });
  };

  return (
    <View style={[appStyles.fulfill, styles.screenContainer]}>
      <StatusBar
        barStyle="light-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <AccentHeader title={yogaClasse.name} backBtn={true} />
      <Text style={styles.instruction}>Select level</Text>
      <CardCarousel data={yogaClasse.levels} />
    </View>
  );
};
