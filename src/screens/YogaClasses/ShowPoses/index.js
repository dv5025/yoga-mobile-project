import React, {useState, useCallback} from 'react';
import {View, StatusBar, Alert} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import AccentHeader from 'src/components/AccentHeader';
import appStyles from 'src/AppStyles';
import styles from './styles';
import PoseList from './PoseList';
import yogaClassApi from '../../../api/yogaClass';
import {useSelector} from 'react-redux';

export default ({route}) => {
  const {levelId} = route.params;
  const {token} = useSelector((state) => state.auth);
  const [isLoading, setIsLoading] = useState(false);

  const [data, setData] = useState({});

  useFocusEffect(
    useCallback(() => {
      getLevelData();
    }, []),
  );

  const getLevelData = () => {
    setIsLoading(true);
    yogaClassApi
      .getYogaLevelById(token, levelId)
      .then((res) => {
        setData({
          yogaClassName: res.data.yogaClass.name,
          poses: res.data.poses.map((item) => {
            return {
              name: item.poses.name,
              image: item.poses.image,
              time: item.time,
            };
          }),
          levelName: res.data.levelName,
        });
      })
      .catch((err) => {
        Alert.alert(err);
      })
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      });
  };

  return (
    <View style={[appStyles.fulfill, styles.screenContainer]}>
      <StatusBar
        barStyle="light-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <AccentHeader
        title={data.yogaClassName + ' - ' + data.levelName}
        backBtn={true}
      />
      <PoseList data={data.poses} />
    </View>
  );
};
