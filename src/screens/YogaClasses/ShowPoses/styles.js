import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  screenContainer: {
    backgroundColor: colors.lightgray,
  },
  flatlist: {
    paddingHorizontal: 8,
  },
  listHeader: {
    height: 8,
  },
  listFooter: {
    height: 8,
  },
  listItem: {
    backgroundColor: '#FFFFFF',
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
  },
  wrapImage: {
    width: 80,
    height: 80,
    borderColor: colors.color_1,
    borderWidth: 2,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 30,
  },
  image: {
    width: 50,
    height: 50,
  },
  poseName: {
    width: 250,
    fontWeight: 'bold',
    fontSize: 13,
  },
  time: {
    fontSize: 13,
    color: '#868686',
  },
});

export default styles;
