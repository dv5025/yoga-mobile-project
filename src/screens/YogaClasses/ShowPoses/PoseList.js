import React from 'react';
import {FlatList, Image} from 'react-native';
import ListDivider from 'src/components/ListDivider';
import EmptyList from 'src/components/EmptyList';
import {View} from 'react-native';
import styles from './styles';
import appStyles from 'src/AppStyles';
import {Text} from 'native-base';
import {poseImageUrl} from '../../../api/index';

const PoseList = ({data}) => {
  return (
    <FlatList
      data={data}
      renderItem={renderItem}
      keyExtractor={(item, index) => index + ''}
      ItemSeparatorComponent={() => <ListDivider />}
      ListEmptyComponent={<EmptyList />}
      style={styles.flatlist}
      ListHeaderComponent={<View style={styles.listHeader} />}
      ListFooterComponent={<View style={styles.listFooter} />}
    />
  );
};

const renderItem = ({item}) => {
  return (
    <View style={styles.listItem}>
      <View style={styles.wrapImage}>
        <Image
          source={{uri: `${poseImageUrl}/${item.image.filename}`}}
          style={styles.image}
        />
      </View>
      <View style={appStyles.fulfill}>
        <Text style={styles.poseName}>{item.name}</Text>
        <Text style={styles.time}>{item.time + ' seconds'}</Text>
      </View>
    </View>
  );
};

export default PoseList;
