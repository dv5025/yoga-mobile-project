import React, {useState, useCallback} from 'react';
import {View, Keyboard, StatusBar, Alert} from 'react-native';
import PrimaryHeader from 'src/components/PrimaryHeader';
import styles from './styles';
import appStyles from 'src/AppStyles';
import {useNavigation} from '@react-navigation/native';
import Step1 from './Step1/Step1';
// import Step2 from './Step2/Step2';
import Step3 from './Step3/Step3';
import {useFocusEffect} from '@react-navigation/native';
import classApi from '../../../api/yogaClass';
import {useSelector} from 'react-redux';

export default () => {
  const navigation = useNavigation();
  const {token} = useSelector((state) => state.auth);
  const [isKeyboardShow, setIsKeyboardShow] = useState(false);

  const [currentStep, setCurrentStep] = useState(1);
  const [isLoading, setIsLoading] = useState(false);

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  // const [coverImage, setCoverImage] = useState('');
  const [poses, setPoses] = useState([]);

  useFocusEffect(
    useCallback(() => {
      Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

      return () => {
        Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
        Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
      };
    }, []),
  );

  const _keyboardDidShow = () => {
    setIsKeyboardShow(true);
  };

  const _keyboardDidHide = () => {
    setIsKeyboardShow(false);
  };

  const nextStep = () => {
    setCurrentStep(currentStep + 1);
  };

  const previousStep = () => {
    setCurrentStep(currentStep - 1);
  };

  const addCustomClass = () => {
    setIsLoading(true);
    classApi
      .createCustomClass(token, getTransferObj())
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
          navigation.goBack();
        }, 500);
      })
      .catch((err) => {
        setIsLoading(false);
        Alert.alert(err);
      });
  };

  const getTransferObj = () => {
    return {
      name,
      description,
      poses,
    };
  };

  const getShowContent = () => {
    if (currentStep === 1) {
      return (
        <Step1
          setName={setName}
          setDescription={setDescription}
          isKeyboardShow={isKeyboardShow}
          nextStep={nextStep}
        />
      );
    }
    // else if (currentStep === 2) {
    //   return (
    //     <Step2
    //       coverImage={coverImage}
    //       setCoverImage={setCoverImage}
    //       nextStep={nextStep}
    //     />
    //   );
    // }
    else {
      return (
        <Step3
          poses={poses}
          setPoses={setPoses}
          isKeyboardShow={isKeyboardShow}
          addCustomClass={addCustomClass}
          isAddingClass={isLoading}
        />
      );
    }
  };

  return (
    <View style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <PrimaryHeader
        backBtn
        subtitleStyle
        onBackBtnPress={
          currentStep === 1 ? () => navigation.goBack() : previousStep
        }
        title="Add custom class"
        containerStyle={styles.header}
        progress={currentStep + ' of 2'}
      />
      <View style={styles.contentContainer}>{getShowContent()}</View>
    </View>
  );
};
