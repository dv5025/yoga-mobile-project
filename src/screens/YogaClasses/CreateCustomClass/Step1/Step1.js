import React, {useRef, useState, Fragment} from 'react';
import {View, StyleSheet} from 'react-native';
import StepHeader from '../StepHeader';
import FormInput from 'src/components/FormInput';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import appStyles from 'src/AppStyles';
import Divider from 'src/components/ListDivider';
import buttonStyles from '../styles';
import Button from 'src/components/Button';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {isInputError, getErrorMessage} from '../../../../helpers/validation';

const Step1 = ({setName, setDescription, isKeyboardShow, nextStep}) => {
  const descriptionInput = useRef();
  const [inputTouched, setInputTouched] = useState({
    name: false,
    description: false,
  });

  const onNextPress = (values) => {
    setName(values.name);
    setDescription(values.description);
    nextStep();
  };

  const isDisableSaveButton = (errprMes, values) => {
    if (values.name === '' || values.description === '') {
      return true;
    } else {
      return errprMes.name || errprMes.description ? true : false;
    }
  };

  const validationSchema = Yup.object().shape({
    name: Yup.string().required('Class name is required'),
    description: Yup.string().required('Description is required'),
  });

  return (
    <View style={appStyles.fulfill}>
      <StepHeader number={1} description="Basic information" />
      <KeyboardAwareScrollView contentContainerStyle={appStyles.scrollView}>
        <Formik
          initialValues={{
            name: '',
            description: '',
          }}
          onSubmit={(values) => onNextPress(values)}
          validationSchema={validationSchema}>
          {({handleChange, values, handleSubmit, errors, touched}) => (
            <Fragment>
              <View style={styles.formItem}>
                <FormInput
                  label="Name"
                  placeholder="name"
                  returnKeyType="next"
                  value={values.name}
                  onChangeText={(text) => {
                    setInputTouched({...inputTouched, name: true});
                    handleChange('name')(text);
                  }}
                  onSubmitEditing={() => descriptionInput.current.focus()}
                  inputError={isInputError(
                    touched.name,
                    errors.name,
                    inputTouched.name,
                  )}
                  errorMessage={getErrorMessage(
                    touched.name,
                    errors.name,
                    inputTouched.name,
                  )}
                  required
                />
              </View>
              <Divider />
              <View style={styles.formItem}>
                <FormInput
                  ref={descriptionInput}
                  label="Description"
                  placeholder="description"
                  value={values.description}
                  onChangeText={(text) => {
                    setInputTouched({...inputTouched, description: true});
                    handleChange('description')(text);
                  }}
                  inputError={isInputError(
                    touched.description,
                    errors.description,
                    inputTouched.description,
                  )}
                  errorMessage={getErrorMessage(
                    touched.description,
                    errors.description,
                    inputTouched.description,
                  )}
                  required
                />
              </View>
              {isKeyboardShow ? (
                <></>
              ) : (
                <View style={buttonStyles.progressButtonPosition}>
                  <Button
                    containerStyle={buttonStyles.progressButtonContainer}
                    gradient={true}
                    label="Next"
                    style={buttonStyles.progressButton}
                    onPress={handleSubmit}
                    disabled={isDisableSaveButton(errors, values)}
                  />
                </View>
              )}
            </Fragment>
          )}
        </Formik>
      </KeyboardAwareScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  formItem: {
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    paddingHorizontal: 8,
    marginHorizontal: 8,
  },
});

export default Step1;
