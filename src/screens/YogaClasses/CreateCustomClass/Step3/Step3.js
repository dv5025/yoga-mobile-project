import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Alert,
} from 'react-native';
import StepHeader from '../StepHeader';
import {Icon} from 'native-base';
import appStyles, {colors} from 'src/AppStyles';
import Divider from 'src/components/ListDivider';
import ModalAddPose from './ModalAddPose';
import ModalOption from './ModalOption';
import LoadingScreen from 'src/components/LoadingScreen';
import buttonStyles from '../styles';
import Button from 'src/components/Button';
import poseApi from '../../../../api/pose';
import {useSelector} from 'react-redux';
import {poseImageUrl} from '../../../../api/index';

const Step3 = ({poses, setPoses, addCustomClass, isAddingClass}) => {
  const {token} = useSelector((state) => state.auth);
  const [modalOptionVisible, setModalOptionVisible] = useState(false);
  const [modalAddPoseVisible, setModalAddPoseVisible] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(-1);

  const [presetPose, setPresetPose] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getPresetPoses();
  }, []);

  const getPresetPoses = () => {
    setIsLoading(true);
    poseApi
      .getAllPoses(token)
      .then((res) => {
        setPresetPose(res.data);
      })
      .catch((err) => {
        Alert.alert(err);
      })
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      });
  };

  const openModalOption = (index) => {
    setSelectedIndex(index);
    setModalOptionVisible(true);
  };

  const onModalOptionClose = (result) => {
    if (result === 'EDIT') {
      setModalAddPoseVisible(true);
    } else if (result === 'DELETE') {
      setPoses(poses.filter((item, index) => index !== selectedIndex));
      setSelectedIndex(-1);
    } else {
      setSelectedIndex(-1);
    }
  };

  const onAddPress = () => {
    if (verifyInput().valid) {
      addCustomClass();
    }
  };

  const verifyInput = () => {
    if (poses.length < 3) {
      Alert.alert('Please add at least 3 poses');
      return {valid: false};
    } else {
      return {valid: true};
    }
  };

  const renderItem = ({item, index}) => {
    const poseTemp = presetPose.find((pose) => pose.id === item.poseId);

    return (
      <View style={poseItemStyles.card}>
        <View style={poseItemStyles.wrapPoseImage}>
          <Image
            source={{uri: `${poseImageUrl}/${poseTemp?.image.filename}`}}
            style={poseItemStyles.poseImage}
          />
        </View>
        <View style={appStyles.fulfill}>
          <Text style={poseItemStyles.poseName}>{poseTemp?.name}</Text>
          <Text style={poseItemStyles.time}>{item.time + ' seconds'}</Text>
        </View>
        <TouchableOpacity onPress={() => openModalOption(index)}>
          <Icon
            type="MaterialCommunityIcons"
            name="dots-vertical"
            style={poseItemStyles.icon}
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={appStyles.fulfill}>
      <View style={appStyles.fulfill}>
        <FlatList
          data={poses}
          renderItem={renderItem}
          keyExtractor={(item, index) => index + ''}
          ItemSeparatorComponent={() => <Divider />}
          ListFooterComponent={() => listFooter(setModalAddPoseVisible)}
          ListHeaderComponent={
            <StepHeader number={2} description="Select your pose" />
          }
        />
      </View>
      <View style={buttonStyles.progressButtonPosition}>
        <Button
          containerStyle={buttonStyles.progressButtonContainer}
          gradient={true}
          label="Add"
          style={buttonStyles.progressButton}
          onPress={onAddPress}
          loading={isAddingClass}
        />
      </View>
      <ModalAddPose
        visible={modalAddPoseVisible}
        setVisible={setModalAddPoseVisible}
        setPoses={setPoses}
        presetPose={presetPose}
        poses={poses}
        selectedIndex={selectedIndex}
        setSelectedIndex={setSelectedIndex}
      />
      <ModalOption
        visible={modalOptionVisible}
        setVisible={setModalOptionVisible}
        onClose={onModalOptionClose}
        setSelectedIndex={setSelectedIndex}
      />
      <LoadingScreen visible={isLoading} />
    </View>
  );
};

const listFooter = (setModalAddPoseVisible) => {
  return (
    <TouchableOpacity
      style={[styles.footerCard, appStyles.center]}
      onPress={() => setModalAddPoseVisible(true)}>
      <Icon type="MaterialCommunityIcons" name="plus" style={styles.plusIcon} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  footerCard: {
    backgroundColor: '#FFFFFF',
    padding: 8,
    height: 100,
    marginHorizontal: 8,
    borderRadius: 8,
    marginTop: 8,
    marginBottom: 72,
  },
  plusIcon: {
    color: colors.color_2,
    fontSize: 55,
  },
});

const poseItemStyles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    marginHorizontal: 8,
  },
  wrapPoseImage: {
    width: 90,
    height: 90,
    borderColor: colors.color_2,
    borderWidth: 3,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 16,
  },
  poseImage: {
    width: 60,
    height: 60,
  },
  poseName: {
    fontWeight: 'bold',
    fontSize: 13,
  },
  time: {
    fontSize: 13,
    color: '#868686',
  },
  icon: {
    color: colors.color_2,
  },
});

export default Step3;
