import React, {useState, useEffect, useRef} from 'react';
import Modal from 'react-native-modal';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  Text,
  ScrollView,
  TextInput,
  Alert,
  FlatList,
  Image,
} from 'react-native';
import appStyles, {colors} from 'src/AppStyles';
import {Icon} from 'native-base';
import Divider from 'src/components/ListDivider';
import Button from 'src/components/Button';
import screenStyle from '../styles';
import EmptyList from 'src/components/EmptyList';
import {poseImageUrl} from '../../../../api/index';

const ModalAddPose = ({
  poses,
  visible,
  setVisible,
  setPoses,
  presetPose,
  selectedIndex,
  setSelectedIndex,
}) => {
  const [isSelectingPose, setIsSelectingPose] = useState(false);

  const [poseId, setPoseId] = useState('');
  const [time, setTime] = useState('0');

  const timeInput = useRef();

  const timeInputOnFocus = () => {
    if (time === '0') {
      setTime('');
    }
  };

  const timeInputOnBlur = () => {
    if (time === '') {
      setTime('0');
    }
  };

  useEffect(() => {
    if (selectedIndex !== -1) {
      setPoseId(poses[selectedIndex].poseId);
      setTime(poses[selectedIndex].time + '');
    } else {
      setPoseId('');
      setTime('0');
    }
  }, [selectedIndex, visible]);

  const hideModal = () => {
    setVisible(false);
    setPoseId('');
    setTime('0');
    setIsSelectingPose(false);
  };

  const increaseTime = () => {
    setTime(parseInt(time) + 1 + '');
  };

  const decreaseTime = () => {
    if (parseInt(time) - 1 >= 0) {
      setTime(parseInt(time) - 1 + '');
    }
  };

  const onDonePress = () => {
    if (selectedIndex !== -1) {
      if (poseId === '') {
        Alert.alert('Please select pose');
      } else if (parseInt(time) < 10) {
        Alert.alert('Time must more than 10 seconds');
      } else {
        setPoses(
          poses.map((item, index) => {
            if (index === selectedIndex) {
              return {poseId, time: parseInt(time)};
            } else {
              return item;
            }
          }),
        );
        setSelectedIndex(-1);
        hideModal();
      }
    } else {
      if (poseId === '') {
        Alert.alert('Please select pose');
      } else if (parseInt(time) < 10) {
        Alert.alert('Time must more than 10 seconds');
      } else {
        setPoses([...poses, {poseId, time: parseInt(time)}]);
        hideModal();
      }
    }
  };

  const cardOnPress = (id) => {
    setPoseId(id);
    setIsSelectingPose(false);
  };

  const renderPostList = ({item}) => {
    return (
      <TouchableOpacity
        style={selectingPoseStyles.card}
        activeOpacity={0.6}
        onPress={() => cardOnPress(item.id)}>
        <View style={selectingPoseStyles.wrapPoseImage}>
          <Image
            source={{uri: `${poseImageUrl}/${item.image.filename}`}}
            style={selectingPoseStyles.poseImage}
          />
        </View>
        <View style={appStyles.fulfill}>
          <Text style={styles.poseName}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <Modal
      isVisible={visible}
      onBackdropPress={hideModal}
      onBackButtonPress={hideModal}
      statusBarTranslucent={true}
      propagateSwipe
      style={styles.modal}>
      <View style={styles.hideBtnContainer}>
        <TouchableOpacity style={styles.wrapHideBtn} onPress={hideModal}>
          <View style={styles.hideBtn} />
        </TouchableOpacity>
      </View>
      <View style={styles.contentContainer}>
        {isSelectingPose ? (
          <>
            <View style={selectingPoseStyles.wrapHeader}>
              <TouchableOpacity
                onPress={() => setIsSelectingPose(false)}
                style={selectingPoseStyles.wrapBackIcon}>
                <Icon
                  type="MaterialCommunityIcons"
                  name="chevron-left"
                  style={selectingPoseStyles.backIcon}
                />
              </TouchableOpacity>
              <View style={[appStyles.center, appStyles.fulfill]}>
                <Text>Select one pose</Text>
              </View>
              <View style={selectingPoseStyles.wrapBackIcon} />
            </View>
            <FlatList
              data={presetPose}
              renderItem={renderPostList}
              keyExtractor={(item, index) => index + ''}
              ListEmptyComponent={
                <EmptyList
                  label="No poses data"
                  iconSource={require('src/assets/empty-poses.png')}
                />
              }
              style={selectingPoseStyles.flatlist}
              ItemSeparatorComponent={() => <Divider />}
              ListFooterComponent={<Divider />}
            />
          </>
        ) : (
          <>
            <ScrollView contentContainerStyle={appStyles.scrollView}>
              <View style={styles.wrapSection}>
                <Text style={styles.title}>Pose</Text>
                {poseId ? (
                  <>
                    <Divider />
                    <TouchableOpacity
                      style={selectingPoseStyles.card}
                      activeOpacity={0.6}
                      onPress={() => setIsSelectingPose(true)}>
                      <View style={selectingPoseStyles.wrapPoseImage}>
                        <Image
                          source={{
                            uri: `${poseImageUrl}/${
                              presetPose.find((item) => item.id === poseId)
                                .image.filename
                            }`,
                          }}
                          style={selectingPoseStyles.poseImage}
                        />
                      </View>
                      <View style={appStyles.fulfill}>
                        <Text style={styles.poseName}>
                          {presetPose.find((item) => item.id === poseId).name}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </>
                ) : (
                  <TouchableOpacity
                    style={styles.addPoseButton}
                    onPress={() => setIsSelectingPose(true)}>
                    <Icon
                      type="MaterialCommunityIcons"
                      name="plus"
                      style={styles.addPoseIcon}
                    />
                    <Text>Select pose</Text>
                  </TouchableOpacity>
                )}
              </View>
              <Divider />
              <View style={styles.wrapSection}>
                <Text style={styles.title}>Time</Text>
                <View style={styles.wrapSelectTime}>
                  <MinusButton onPress={decreaseTime} />
                  <TouchableOpacity
                    style={styles.timeScreen}
                    onPress={() => timeInput.current.focus()}>
                    <TextInput
                      ref={timeInput}
                      onFocus={timeInputOnFocus}
                      onBlur={timeInputOnBlur}
                      style={styles.time}
                      value={time}
                      onChangeText={(text) => setTime(text)}
                      keyboardType="number-pad"
                    />
                    <Text>seconds</Text>
                  </TouchableOpacity>
                  <PlusButton onPress={increaseTime} />
                </View>
              </View>
            </ScrollView>
            <View style={screenStyle.progressButtonPosition}>
              <Button
                containerStyle={screenStyle.progressButtonContainer}
                gradient={true}
                label="Done"
                style={screenStyle.progressButton}
                onPress={onDonePress}
              />
            </View>
          </>
        )}
      </View>
    </Modal>
  );
};

const MinusButton = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.button}>
      <Icon type="MaterialCommunityIcons" name="minus" style={styles.icon} />
    </TouchableOpacity>
  );
};

const PlusButton = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.button}>
      <Icon type="MaterialCommunityIcons" name="plus" style={styles.icon} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'flex-end',
    padding: 0,
    margin: 0,
    marginTop: StatusBar.currentHeight + 30,
  },
  hideBtnContainer: {
    backgroundColor: colors.lightgray,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  wrapHideBtn: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingVertical: 10,
  },
  hideBtn: {
    height: 5,
    width: 40,
    backgroundColor: '#C6C6C6',
    borderRadius: 10,
  },
  contentContainer: {
    backgroundColor: colors.lightgray,
    height: '90%',
  },
  wrapSection: {
    padding: 8,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  addPoseButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    paddingVertical: 16,
    marginTop: 8,
  },
  addPoseIcon: {
    color: colors.color_2,
  },
  wrapSelectTime: {
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    marginTop: 8,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 8,
  },
  button: {
    backgroundColor: colors.color_3,
    height: 60,
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
  },
  icon: {
    color: '#FFFFFF',
  },
  timeScreen: {
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(120,209,210,0.1)',
    flex: 1,
    marginHorizontal: 8,
    borderRadius: 8,
  },
  time: {
    color: '#6F6F6F',
    fontSize: 30,
    marginBottom: -15,
    marginTop: -10,
    textAlign: 'center',
  },
  poseName: {
    fontWeight: 'bold',
    fontSize: 13,
  },
});

const selectingPoseStyles = StyleSheet.create({
  wrapHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrapBackIcon: {
    marginLeft: 8,
    width: 50,
  },
  backIcon: {
    color: '#6F6F6F',
  },
  card: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    paddingRight: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
  },
  wrapPoseImage: {
    width: 90,
    height: 90,
    borderColor: colors.color_2,
    borderWidth: 3,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 30,
  },
  poseImage: {
    width: 60,
    height: 60,
  },
  flatlist: {
    flex: 1,
    paddingHorizontal: 8,
    marginTop: 8,
  },
});

export default ModalAddPose;
