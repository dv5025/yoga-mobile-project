import React from 'react';
import {Modal, View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import {Icon} from 'native-base';

const ModalOption = ({visible, setVisible, onClose}) => {
  const editPressed = () => {
    onClose('EDIT');
    setVisible(false);
  };

  const deletePressed = () => {
    onClose('DELETE');
    setVisible(false);
  };

  const handleOnReqClose = () => {
    onClose('CANCEL');
    setVisible(false);
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={visible}
      statusBarTranslucent={true}
      onRequestClose={handleOnReqClose}>
      <View style={styles.modalContainer}>
        <TouchableOpacity
          style={styles.modalBackground}
          onPress={handleOnReqClose}
        />
        <View style={styles.modalView}>
          <TouchableOpacity style={styles.menuItem} onPress={editPressed}>
            <Icon
              type="MaterialCommunityIcons"
              name="pencil"
              style={styles.icon}
            />
            <Text style={styles.editText}>Edit</Text>
          </TouchableOpacity>
          <View style={styles.divider} />
          <TouchableOpacity style={styles.menuItem} onPress={deletePressed}>
            <Icon
              type="MaterialCommunityIcons"
              name="trash-can-outline"
              style={[styles.icon, styles.deleteIcon]}
            />
            <Text style={styles.deleteText}>Delete</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalBackground: {
    flex: 1,
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
  },
  modalView: {
    position: 'absolute',
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  menuItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
    paddingVertical: 10,
    width: 300,
  },
  icon: {
    marginRight: 15,
    color: '#494949',
  },
  editText: {
    color: '#494949',
  },
  divider: {
    height: 1,
    backgroundColor: '#D5D5D5',
  },
  deleteIcon: {
    color: '#E74C4C',
  },
  deleteText: {
    color: '#E74C4C',
  },
});

export default ModalOption;
