import React from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import StepHeader from '../StepHeader';
import ImagePicker from 'src/components/ImagePicker';
import appStyles from 'src/AppStyles';
import buttonStyles from '../styles';
import Button from 'src/components/Button';

const Step2 = ({coverImage, setCoverImage, nextStep}) => {
  const onNextPress = () => {
    if (verifyInput().valid) {
      nextStep();
    }
  };

  const verifyInput = () => {
    if (coverImage === '') {
      Alert.alert('Please select cover image');
      return {valid: false};
    } else {
      return {valid: true};
    }
  };

  return (
    <View style={appStyles.fulfill}>
      <StepHeader number={2} description="Select your cover image" />
      <View style={styles.wrapImagePicker}>
        <ImagePicker
          source={
            coverImage === ''
              ? require('src/assets/images.png')
              : {
                  uri: coverImage.path,
                }
          }
          onSelected={(res) => setCoverImage(res)}
          editIcon="square-edit-outline"
        />
      </View>
      <View style={buttonStyles.progressButtonPosition}>
        <Button
          containerStyle={buttonStyles.progressButtonContainer}
          gradient={true}
          label="Next"
          style={buttonStyles.progressButton}
          onPress={onNextPress}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapImagePicker: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Step2;
