import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFFFFF',
  },
  safeAreaView: {
    backgroundColor: colors.lightgray,
  },
  contentContainer: {
    flex: 1,
  },
  progressButtonPosition: {
    alignItems: 'center',
    width: '100%',
    position: 'absolute',
    bottom: 10,
  },
  progressButtonContainer: {
    width: 150,
  },
  progressButton: {
    borderRadius: 100,
  },
});

export default styles;
