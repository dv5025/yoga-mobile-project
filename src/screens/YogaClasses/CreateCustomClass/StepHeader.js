import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const StepHeader = ({number, description}) => {
  return (
    <View style={styles.container}>
      <View style={styles.circle}>
        <Text style={styles.number}>{number}</Text>
      </View>
      <Text style={styles.description}>{description}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 24,
    paddingBottom: 16,
  },
  circle: {
    backgroundColor: colors.color_2,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  number: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 20,
  },
  description: {
    marginTop: 8,
  },
});

export default StepHeader;
