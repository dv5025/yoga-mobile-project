import {StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: colors.lightgray,
  },
  wrapHeader: {
    marginHorizontal: 8,
  },
  wrapSegment: {
    marginTop: 16,
  },
  flatlist: {
    flex: 1,
    paddingTop: 5,
    paddingHorizontal: 8,
  },
  wrapListHeaderComponent: {
    marginTop: 8,
  },
  plusIcon: {
    color: colors.color_3,
    fontSize: 80,
  },
  divider: {
    height: 8,
  },
  footer: {
    height: 16,
  },
});

export default styles;
