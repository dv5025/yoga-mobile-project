import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {colors} from 'src/AppStyles';
// import {useNavigation} from '@react-navigation/native';

const Card = ({name, description, image, type, id}) => {
  // const navigation = useNavigation();

  return (
    <View style={styles.card}>
      <View style={styles.cardImageContainer}>
        <Image source={image} style={styles.cardImage} />
      </View>
      <View style={styles.cardHeader}>
        <Text style={styles.className}>{name}</Text>
        <Text style={[styles.description]} numberOfLines={2}>
          {description}
        </Text>
      </View>
      {/* {type === 'CUSTOM' ? (
        <TouchableOpacity
          style={styles.startButton}
          onPress={() => navigation.navigate('DoingYoga', {customClassId: id})}>
          <Text style={styles.startButtonLabel}>Start</Text>
        </TouchableOpacity>
      ) : (
        <></>
      )} */}
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#FFFFFF',
    borderRadius: 16,
    borderColor: '#ECECEC',
    borderWidth: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardImageContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    width: 130,
    height: 130,
  },
  cardImage: {
    width: 110,
    height: 110,
  },
  cardHeader: {
    justifyContent: 'center',
    paddingRight: 16,
    paddingLeft: 8,
    flex: 1,
  },
  className: {
    fontWeight: 'bold',
    color: '#616161',
    fontSize: 16,
  },
  description: {
    fontSize: 12,
    marginTop: 5,
    color: '#909090',
  },
  startButton: {
    backgroundColor: colors.color_1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    width: 60,
    borderRadius: 30,
    marginRight: 16,
    borderColor: colors.color_2,
    borderWidth: 2,
  },
  startButtonLabel: {
    color: '#FFFFFF',
    fontWeight: 'bold',
  },
});

export default Card;
