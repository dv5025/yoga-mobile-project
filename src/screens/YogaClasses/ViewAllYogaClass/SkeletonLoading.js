import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {View, StyleSheet} from 'react-native';

const SkeletonLoading = () => {
  return (
    <SkeletonPlaceholder>
      <View style={styles.container}>
        <View style={styles.image} />
        <View>
          <View style={[{width: 200, height: 20}, styles.text]} />
          <View style={[{width: 150}, styles.text, styles.description]} />
          <View style={[{width: 100}, styles.text, styles.description]} />
          <View style={[{width: 80}, styles.text, styles.description]} />
        </View>
      </View>
    </SkeletonPlaceholder>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    borderRadius: 16,
    borderColor: '#ECECEC',
    borderWidth: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
    height: 130,
    padding: 16,
  },
  text: {
    borderRadius: 4,
  },
  description: {
    height: 10,
    marginTop: 6,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 4,
    marginRight: 16,
  },
});

export default SkeletonLoading;
