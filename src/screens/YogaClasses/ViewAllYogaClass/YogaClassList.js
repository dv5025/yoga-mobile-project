import React from 'react';
import {FlatList, TouchableOpacity, View, Alert} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';
import Card from './Card';
import EmptyList from 'src/components/EmptyList';
import {yogaClassCoverImageUrl} from '../../../api/index';
import SkeletonLoading from './SkeletonLoading';

const YogaClassList = ({data, isLoading}) => {
  const navigation = useNavigation();

  const cardOnPress = (yogaClass) => {
    navigation.navigate('SelectLevel', {yogaClassId: yogaClass.id});
  };

  const customCardOnPress = (yogaClass) => {
    Alert.alert(
      'Are you ready ?',
      '',
      [
        {
          text: 'NO',
          onPress: () => null,
          style: 'cancel',
        },
        {
          text: 'YES',
          onPress: () =>
            navigation.navigate('DoingYoga', {customClassId: yogaClass.id}),
        },
      ],
      {cancelable: false},
    );
  };

  const renderItem = ({item}) => {
    return isLoading ? (
      <SkeletonLoading />
    ) : (
      <TouchableOpacity
        activeOpacity={item.type === 'CUSTOM' ? 1 : 0.6}
        onPress={
          item.type === 'CUSTOM'
            ? () => customCardOnPress(item)
            : () => cardOnPress(item)
        }>
        <Card
          name={item.name}
          description={item.description}
          image={
            item.image.filename
              ? {uri: `${yogaClassCoverImageUrl}/${item.image.filename}`}
              : require('src/assets/logo.png')
          }
          type={item.type}
          id={item.id}
        />
      </TouchableOpacity>
    );
  };

  return (
    <FlatList
      data={isLoading ? new Array(3) : data}
      renderItem={renderItem}
      keyExtractor={(item, index) => index + ''}
      ListEmptyComponent={
        <EmptyList
          label="No class data"
          iconSource={require('src/assets/empty-poses.png')}
        />
      }
      style={styles.flatlist}
      ListHeaderComponent={<View style={styles.wrapListHeaderComponent} />}
      ItemSeparatorComponent={divider}
      ListFooterComponent={footer}
    />
  );
};

const divider = () => {
  return <View style={styles.divider} />;
};

const footer = <View style={styles.footer} />;

export default YogaClassList;
