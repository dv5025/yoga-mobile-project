import React, {useState, useCallback} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {StatusBar, BackHandler, View, Alert} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import styles from './styles';
import YogaClassList from './YogaClassList';
import appStyles from 'src/AppStyles';
import exitApp from 'src/helpers/exitApp';
import SearchButton from 'src/components/SearchButton';
import SimpleHeader from 'src/components/SimpleHeader';
import SearchBar from 'src/components/SearchBar';
import Segment, {SegmentItem} from 'src/components/Segment';
import AddBtn from 'src/components/AddBtn';
import {useNavigation} from '@react-navigation/native';
import yogaClassApi from '../../../api/yogaClass';
import {useSelector} from 'react-redux';

export default () => {
  const navigation = useNavigation();
  const {token} = useSelector((state) => state.auth);

  const [isLoading, setIsLoading] = useState(false);
  const [yogaClasses, setYogaClasses] = useState([]);
  const [isShowSearchBar, setIsShowSearchBar] = useState(false);
  const [currentSection, setCurrentSection] = useState('PRESET');

  const [searchKeyword, setSearchKeyword] = useState('');

  useFocusEffect(
    useCallback(() => {
      getAllYogaClasses();

      const backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        () => exitApp(BackHandler),
      );

      return () => backHandler.remove();
    }, [currentSection]),
  );

  const getAllYogaClasses = () => {
    setIsLoading(true);
    if (currentSection === 'PRESET') {
      yogaClassApi
        .getAllPresetClass(token)
        .then((response) => {
          setYogaClasses(
            response.data.map((item) => {
              return {
                id: item.id,
                name: item.name,
                image: item.image,
                description: item.description,
                type: item.type,
              };
            }),
          );
        })
        .catch((error) => {
          Alert.alert(error);
        })
        .then(() => {
          setTimeout(() => {
            setIsLoading(false);
          }, 500);
        });
    } else {
      yogaClassApi
        .getUserCustomClasses(token)
        .then((response) => {
          setYogaClasses(
            response.data.map((item) => {
              return {
                id: item.id,
                name: item.name,
                image: item.image,
                description: item.description,
                type: item.type,
              };
            }),
          );
        })
        .catch((error) => {
          Alert.alert(error);
        })
        .then(() => {
          setTimeout(() => {
            setIsLoading(false);
          }, 500);
        });
    }
  };

  const onSelectSection = (value) => {
    setCurrentSection(value);
  };

  return (
    <SafeAreaView style={[appStyles.fulfill, styles.safeAreaView]}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <View style={styles.wrapHeader}>
        <SimpleHeader
          title="Classes"
          description="Focus on flexibility, strength, balance. Find your favorite class."
          right={
            <SearchButton
              onPress={() => {
                setSearchKeyword('');
                setIsShowSearchBar(!isShowSearchBar);
              }}
            />
          }
        />
        <View style={[styles.wrapSegment, appStyles.center]}>
          <Segment initialValue="PRESET" onSelected={onSelectSection}>
            <SegmentItem value="PRESET">Preset</SegmentItem>
            <SegmentItem value="CUSTOM">Custom</SegmentItem>
          </Segment>
        </View>
        <SearchBar
          placeholder="Search by class name"
          onSubmitEditing={() => null}
          visible={isShowSearchBar}
          onChangeText={(text) => setSearchKeyword(text)}
        />
      </View>
      <YogaClassList
        data={
          searchKeyword
            ? yogaClasses.filter((item) => item.name.match(searchKeyword))
            : yogaClasses
        }
        isShowSearchBar={isShowSearchBar}
        setIsShowSearchBar={setIsShowSearchBar}
        isLoading={isLoading}
      />
      {currentSection === 'CUSTOM' ? (
        <AddBtn onPress={() => navigation.navigate('AddClass')} />
      ) : (
        <></>
      )}
      {/* <LoadingScreen visible={isLoading} /> */}
    </SafeAreaView>
  );
};
