import {host} from './index';
import Axios from 'axios';

const getAllPoses = (token) => {
  return Axios.get(`${host}/poses`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export default {getAllPoses};
