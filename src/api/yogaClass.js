import {host} from './index';
import Axios from 'axios';

const getAllPresetClass = (token) => {
  return Axios.get(`${host}/yogaClasses`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const getPresetClassById = (token, id) => {
  return Axios.get(`${host}/yogaClasses/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const getUserCustomClasses = (token) => {
  return Axios.get(`${host}/customClass/user`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const getCustomClassById = (token, id) => {
  return Axios.get(`${host}/customClasses/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const getYogaLevelById = (token, id) => {
  return Axios.get(`${host}/yogaClass/level/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const createCustomClass = (token, data) => {
  return Axios.post(
    `${host}/customClass`,
    {...data},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

export default {
  getAllPresetClass,
  getPresetClassById,
  getUserCustomClasses,
  getCustomClassById,
  getYogaLevelById,
  createCustomClass,
};
