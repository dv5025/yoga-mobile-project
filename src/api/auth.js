import {host} from './index';
import Axios from 'axios';

const login = (data) => {
  return Axios.post(`${host}/auth/login`, data);
};

const logout = (token) => {
  return Axios.post(
    `${host}/auth/logout`,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const register = (data) => {
  return Axios.post(`${host}/auth/signup`, data);
};

const checkUsernameAvailability = (username) => {
  return Axios.get(
    `${host}/auth/checkUsernameAvailability?username=${username}`,
  );
};

const checkEmailAvailability = (email) => {
  return Axios.get(`${host}/auth/checkEmailAvailability?email=${email}`);
};

export default {
  login,
  logout,
  register,
  checkUsernameAvailability,
  checkEmailAvailability,
};
