import {host} from './index';
import Axios from 'axios';

const getUserHistories = (token) => {
  return Axios.get(`${host}/history`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const addHistory = (token, classId, levelId) => {
  return Axios.post(
    `${host}/history`,
    {
      classId,
      levelId,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

export default {getUserHistories, addHistory};
