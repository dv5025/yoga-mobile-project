import {host} from './index';
import Axios from 'axios';

const getAllPost = (token) => {
  return Axios.get(`${host}/posts`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const getPostById = (token, id) => {
  return Axios.get(`${host}/posts/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const getUserPosts = (token) => {
  return Axios.get(`${host}/post/user`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const addComment = (token, message, postId) => {
  return Axios.post(
    `${host}/comment`,
    {
      message,
      postId,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const addPost = (token, data) => {
  return Axios.post(`${host}/posts`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const editPost = (token, data) => {
  return Axios.patch(`${host}/posts`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const deletePostById = (token, id) => {
  return Axios.delete(`${host}/posts?postId=${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export default {
  getAllPost,
  getPostById,
  getUserPosts,
  addComment,
  addPost,
  editPost,
  deletePostById,
};
