export const host = 'https://yoga-api.twilightparadox.com';
export const profileImageUrl = `${host}/user/profileImage`;
export const yogaClassCoverImageUrl = `${host}/yogaClassCoverImg`;
export const poseImageUrl = `${host}/posesAssets/images`;
export const poseVdoUrl = `${host}/posesAssets/vdos`;
export const postImageUrl = `${host}/postImg`;
