import {host} from './index';
import Axios from 'axios';

const getUserProfile = (token) => {
  return Axios.get(`${host}/user/profile`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const editProfileImage = (token, image) => {
  return Axios.patch(`${host}/user/profileImage`, image, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const editUserProfile = (token, data) => {
  return Axios.patch(
    `${host}/user/profile`,
    {...data},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const editUserPassword = (token, data) => {
  return Axios.patch(
    `${host}/user/password`,
    {...data},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

export default {
  getUserProfile,
  editProfileImage,
  editUserProfile,
  editUserPassword,
};
