import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import MainRouter from 'src/router/index';
import SplashScreen from 'react-native-splash-screen';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider as ReduxProvider} from 'react-redux';
import storeConfig from './store/configureStore';

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <ReduxProvider store={storeConfig.store}>
      <PersistGate loading={null} persistor={storeConfig.persistor}>
        <NavigationContainer>
          <SafeAreaProvider>
            <MainRouter />
          </SafeAreaProvider>
        </NavigationContainer>
      </PersistGate>
    </ReduxProvider>
  );
};

export default App;
