import React from 'react';
import * as Progress from 'react-native-progress';

const ProgressBar = ({progress, width}) => {
  return (
    <Progress.Bar
      progress={progress ? progress : 50}
      width={width ? width : 80}
      borderWidth={0}
      unfilledColor="#FFFFFF"
      color="#78D1D2"
    />
  );
};

export default ProgressBar;
