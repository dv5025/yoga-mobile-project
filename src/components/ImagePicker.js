import React from 'react';
import {View, Image, TouchableOpacity, StyleSheet, Alert} from 'react-native';
import {Icon} from 'native-base';
import {
  check,
  PERMISSIONS,
  RESULTS,
  request,
  openSettings,
} from 'react-native-permissions';
import ImagePicker from 'react-native-image-picker';
import ImageCrop from 'react-native-image-crop-picker';

const ProfileImage = ({source, onSelected, imageStyle, editIcon}) => {
  return (
    <View style={styles.wrapImage}>
      <Image source={source} style={[styles.image, imageStyle]} />
      <TouchableOpacity
        style={styles.wrapCameraIcon}
        onPress={() => checkPermission(onSelected)}>
        <Icon
          type="MaterialCommunityIcons"
          name={editIcon ? editIcon : 'camera'}
          style={styles.cameraIcon}
        />
      </TouchableOpacity>
    </View>
  );
};

export const checkPermission = (onSelected, width, height) => {
  check(PERMISSIONS.ANDROID.CAMERA)
    .then((result) => {
      if (result === RESULTS.GRANTED) {
        openImagePicker(onSelected, width, height);
      }
      if (result === RESULTS.DENIED) {
        request(PERMISSIONS.ANDROID.CAMERA).then((result) => {
          if (result === RESULTS.GRANTED) {
            openImagePicker(onSelected, width, height);
          } else if (result === RESULTS.DENIED) {
            Alert.alert('Please allow to access your camera.');
          } else if (result === RESULTS.BLOCKED) {
            Alert.alert('Go to setting and allow the camera.');
          }
        });
      }
      if (result === RESULTS.BLOCKED) {
        Alert.alert(
          'Permission',
          'Please allow the camera.',
          [
            {
              text: 'Open settings',
              onPress: () =>
                openSettings().catch(() =>
                  console.warn('Cannot open settings'),
                ),
            },
            {text: 'OK', onPress: () => null},
          ],
          {cancelable: false},
        );
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export const openImagePicker = (onSelected, width, height) => {
  ImagePicker.showImagePicker({}, (response) => {
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else {
      ImageCrop.openCropper({
        path: response.uri,
        width: width ? width : 800,
        height: height ? height : 800,
      }).then((image) => {
        onSelected(image);
      });
    }
  });
};

const styles = StyleSheet.create({
  wrapImage: {
    marginVertical: 10,
  },
  image: {
    width: 160,
    height: 160,
    borderRadius: 100,
    borderColor: '#FFFFFF',
    borderWidth: 3,
  },
  wrapCameraIcon: {
    position: 'absolute',
    right: 5,
    top: 5,
    backgroundColor: '#FFFFFF',
    borderRadius: 100,
    padding: 5,
  },
  cameraIcon: {
    color: '#3C3C3C',
  },
});

export default ProfileImage;
