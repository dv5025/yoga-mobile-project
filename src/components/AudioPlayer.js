import SoundPlayer from 'react-native-sound-player';

const playBackgroundSound = () => {
  SoundPlayer.setVolume(0.5);
  SoundPlayer.addEventListener(
    'FinishedPlaying',
    () => {
      try {
        SoundPlayer.playSoundFile('length_of_light', 'mp3');
      } catch (e) {
        console.log('cannot play the sound file', e);
      }
    },
    true,
  );
  SoundPlayer.playSoundFile('length_of_light', 'mp3');
};

const stop = () => {
  SoundPlayer.stop();
};

export default {
  stop,
  playBackgroundSound,
};
