import React from 'react';
import {View, StyleSheet, TouchableHighlight} from 'react-native';
import {Icon, Text} from 'native-base';

const ListItemWithIcon = ({icon, iconColor, name, rightIcon, onPress}) => {
  const styles = StyleSheet.create({
    container: {
      backgroundColor: '#FFFFFF',
      borderRadius: 8,
      marginHorizontal: 10,
      flexDirection: 'row',
      alignItems: 'center',
      padding: 10,
    },
    wrapLeftIcon: {
      width: 40,
    },
    leftIcon: {
      color: iconColor ? iconColor : '#A4A4A4',
      fontSize: 30,
    },
    wrapName: {
      flex: 1,
      paddingHorizontal: 10,
    },
  });

  return (
    <TouchableHighlight
      style={styles.container}
      onPress={onPress}
      underlayColor="#E7E7E7">
      <>
        {icon ? (
          <View style={styles.wrapLeftIcon}>
            <Icon
              type="MaterialCommunityIcons"
              name={icon}
              style={styles.leftIcon}
            />
          </View>
        ) : (
          <></>
        )}
        <View style={styles.wrapName}>
          <Text>{name}</Text>
        </View>
        {rightIcon}
      </>
    </TouchableHighlight>
  );
};

export default ListItemWithIcon;
