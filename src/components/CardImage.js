import React, {useState} from 'react';
import {Image, View, StyleSheet, TouchableOpacity, Modal} from 'react-native';
import {
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
} from 'native-base';
import appStyle from 'src/AppStyles';
import moment from 'moment';

const CardImage = ({
  userImage,
  title,
  createDate,
  image,
  description,
  numberOfLinesDescription,
  numComment,
  creator,
  showOptionButton,
  onEditPressed,
  onDeletePressed,
  cardOnPress,
  commentOnpress,
  style,
}) => {
  const [modalVisible, setModalVisible] = useState(false);

  const editPressed = () => {
    if (onEditPressed) {
      onEditPressed();
    }
    setModalVisible(false);
  };

  const deletePressed = () => {
    if (onDeletePressed) {
      onDeletePressed();
    }
    setModalVisible(false);
  };

  return (
    <View style={style}>
      <Card>
        <CardItem>
          <Left>
            <Thumbnail
              source={userImage ? userImage : require('src/assets/user.png')}
            />
            <Body>
              <Text numberOfLines={1} style={{fontWeight: 'bold'}}>
                {title}
              </Text>
              <Text note>{moment(createDate).format('MMM D, YYYY')}</Text>
            </Body>
          </Left>
          {showOptionButton ? (
            <TouchableOpacity onPress={() => setModalVisible(true)}>
              <Icon type="MaterialCommunityIcons" name="dots-vertical" />
            </TouchableOpacity>
          ) : (
            <></>
          )}
        </CardItem>
        <CardItem>
          <TouchableOpacity
            activeOpacity={cardOnPress ? 0.5 : 1}
            onPress={cardOnPress}>
            <Body>
              {image != null ? (
                <View style={appStyle.center}>
                  <Image
                    resizeMode="stretch"
                    source={image}
                    style={styles.image}
                  />
                </View>
              ) : (
                <></>
              )}

              <Text
                numberOfLines={
                  numberOfLinesDescription
                    ? numberOfLinesDescription
                    : undefined
                }>
                {description}
              </Text>
            </Body>
          </TouchableOpacity>
        </CardItem>
        <CardItem>
          <Left>
            <Button transparent onPress={commentOnpress}>
              <Icon active name="chatbubbles" />
              <Text>{numComment + ' Comments'}</Text>
            </Button>
          </Left>
          <Right>
            <Text style={styles.createBy}>{'created by @' + creator}</Text>
          </Right>
        </CardItem>
      </Card>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        statusBarTranslucent={true}
        onRequestClose={() => setModalVisible(false)}>
        <View style={styles.modalContainer}>
          <TouchableOpacity
            style={styles.modalBackground}
            onPress={() => setModalVisible(false)}
          />
          <View style={styles.modalView}>
            {/* <TouchableOpacity style={styles.menuItem} onPress={editPressed}>
              <Icon
                type="MaterialCommunityIcons"
                name="pencil"
                style={styles.icon}
              />
              <Text style={styles.editText}>Edit</Text>
            </TouchableOpacity>
            <View style={styles.divider} /> */}
            <TouchableOpacity style={styles.menuItem} onPress={deletePressed}>
              <Icon
                type="MaterialCommunityIcons"
                name="trash-can-outline"
                style={[styles.icon, styles.deleteIcon]}
              />
              <Text style={styles.deleteText}>Delete</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  createBy: {
    fontSize: 13,
    color: '#838383',
  },
  image: {
    height: 207,
    width: 368,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalBackground: {
    flex: 1,
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
  },
  modalView: {
    position: 'absolute',
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  menuItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
    paddingVertical: 10,
    width: 300,
  },
  divider: {
    height: 1,
    backgroundColor: '#D5D5D5',
  },
  icon: {
    marginRight: 15,
    color: '#494949',
  },
  deleteIcon: {
    color: '#E74C4C',
  },
  deleteText: {
    color: '#E74C4C',
  },
  editText: {
    color: '#494949',
  },
});

export default CardImage;
