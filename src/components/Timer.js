import React, {useEffect, useState} from 'react';
import CircleProgressBar from './CircleProgressBar';
import {StyleSheet} from 'react-native';
import Tts from 'react-native-tts';

const Timer = ({time, onTimeUp, size, thickness}) => {
  Tts.setDucking(true);
  const [currentTime, setCurrentTime] = useState(0);
  const [isDone, setIsDone] = useState(false);

  useEffect(() => {
    if (currentTime < time) {
      setTimeout(() => {
        setCurrentTime(currentTime + 1);
      }, 1000);
    } else {
      setIsDone(true);
      setTimeout(() => {
        onTimeUp();
      }, 1000);
    }
  }, [currentTime]);

  const getTime = () => {
    if (time - currentTime === 0) {
      Tts.stop();
      Tts.speak('good job!');
    } else if (time - currentTime <= 3) {
      Tts.stop();
      Tts.speak(time - currentTime + '');
    }
    return isDone ? 'Done' : time - currentTime;
  };

  const calProgress = () => {
    return isDone ? 1 : currentTime / time;
  };

  return (
    <CircleProgressBar
      progress={currentTime === 0 ? -1 : calProgress()}
      size={size ? size : 80}
      thickness={thickness ? thickness : 5}
      showsText
      formatText={getTime}
      textStyle={styles.timeText}
    />
  );
};

const styles = StyleSheet.create({
  timeText: {
    fontWeight: 'bold',
  },
});

export default Timer;
