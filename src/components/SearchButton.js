import React from 'react';
import CircleIcon from 'src/components/CircleIcon';
import {TouchableOpacity} from 'react-native';

const SearchButton = ({onPress}) => {
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={onPress}>
      <CircleIcon name="magnify" />
    </TouchableOpacity>
  );
};

export default SearchButton;
