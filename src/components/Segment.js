import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {colors} from 'src/AppStyles';

const Segment = ({children, onSelected, initialValue, color, itemWidth}) => {
  const [selectedValue, setSelectedValue] = useState(initialValue);

  const onOptionPress = (value) => {
    setSelectedValue(value);
    if (onSelected) {
      onSelected(value);
    }
  };

  const backgroundColor = {backgroundColor: color ? color : colors.color_2};
  const item_Width = {width: itemWidth ? itemWidth : 100};

  return (
    <View style={[styles.container, backgroundColor]}>
      {children.map((item, index) => {
        return (
          <TouchableOpacity
            onPress={() => onOptionPress(item.props.value)}
            key={index}
            style={[
              selectedValue === item.props.value
                ? styles.selectedItem
                : styles.unselectedItem,
              item_Width,
              styles.item,
            ]}>
            <Text
              style={
                selectedValue === item.props.value
                  ? styles.selectedText
                  : styles.unselectedText
              }>
              {item.props.children}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export const SegmentItem = ({value, children}) => {
  return <Text>{children}</Text>;
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  item: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    padding: 3,
    margin: 2,
  },
  selectedItem: {
    backgroundColor: '#FFFFFF',
  },
  unselectedItem: {
    backgroundColor: 'rgba(0,0,0,0)',
  },
  selectedText: {
    color: colors.color_2,
  },
  unselectedText: {
    color: '#FFFFFF',
  },
});

export default Segment;
