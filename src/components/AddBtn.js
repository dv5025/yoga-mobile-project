import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {colors} from 'src/AppStyles';
import {Icon} from 'native-base';

const AddBtn = ({onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={styles.container}
      onPress={onPress}>
      <Icon type="MaterialCommunityIcons" name="plus" style={styles.icon} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 60,
    height: 60,
    right: 16,
    bottom: 16,
    backgroundColor: colors.color_3,
    borderRadius: 200,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    color: '#FFFFFF',
    fontSize: 30,
  },
});

export default AddBtn;
