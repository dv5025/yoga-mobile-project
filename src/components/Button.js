import React from 'react';
import {TouchableOpacity, Text, StyleSheet, View} from 'react-native';
import {Spinner} from 'native-base';
import {colors as appColors} from 'src/AppStyles';

const Button = ({
  disabled,
  color,
  onPress,
  label,
  labelStyle,
  style,
  containerStyle,
  loading,
}) => {
  const getColor = () => {
    if (disabled) {
      return '#B8B8B8';
    } else if (color) {
      return color;
    } else {
      return appColors.color_3;
    }
  };

  return (
    <TouchableOpacity
      activeOpacity={disabled ? 1 : 0.5}
      onPress={disabled ? () => null : onPress}
      style={[styles.container, containerStyle]}>
      <View style={[styles.button, style, {backgroundColor: getColor()}]}>
        {loading ? (
          <Spinner color="#FFFFFF" />
        ) : (
          <Text
            style={
              disabled ? styles.disabledLabel : [styles.label, labelStyle]
            }>
            {label}
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 8,
  },
  label: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 16,
  },
  disabledLabel: {
    color: '#8E8E8E',
    fontSize: 16,
  },
});

export default Button;
