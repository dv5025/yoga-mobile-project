import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Icon} from 'native-base';

const CircleIcon = ({name, type, size, color, backgroundColor}) => {
  const styles = StyleSheet.create({
    container: {
      backgroundColor: backgroundColor ? backgroundColor : '#D9D9D9',
      padding: 5,
      borderRadius: 200,
    },
    icon: {
      fontSize: size ? size : 23,
      color: color ? color : '#393939',
    },
  });

  return (
    <View style={styles.container}>
      <Icon
        type={type ? type : 'MaterialCommunityIcons'}
        name={name}
        style={styles.icon}
      />
    </View>
  );
};

export default CircleIcon;
