import React from 'react';
import * as Progress from 'react-native-progress';

const CircleProgressBar = ({progress, size, thickness, ...rest}) => {
  return (
    <Progress.Circle
      {...rest}
      progress={progress ? progress : 0.2}
      size={size ? size : 30}
      thickness={thickness ? thickness : 3}
      borderWidth={0}
      unfilledColor="#C4C4C4"
      color="#78D1D2"
    />
  );
};

export default CircleProgressBar;
