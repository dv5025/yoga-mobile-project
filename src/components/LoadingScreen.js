import React from 'react';
import {Modal, View, StyleSheet} from 'react-native';
import appStyles, {colors} from 'src/AppStyles';
import {Spinner} from 'native-base';

const LoadingScreen = ({visible}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={visible}
      statusBarTranslucent={true}
      onRequestClose={() => null}>
      <View style={[appStyles.center, styles.modalBackground]}>
        <Spinner color={colors.color_2} size={60} />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
  },
});

export default LoadingScreen;
