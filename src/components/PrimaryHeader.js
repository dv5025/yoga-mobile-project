import React from 'react';
import {View, Text, Icon} from 'native-base';
import {StyleSheet, TouchableOpacity, StatusBar} from 'react-native';
import appStyles from 'src/AppStyles';
import {colors} from 'src/AppStyles';
import {useNavigation} from '@react-navigation/native';

const PrimaryHeader = ({
  backBtn,
  onBackBtnPress,
  title,
  progress,
  subtitleStyle,
  right,
  containerStyle,
}) => {
  const navigation = useNavigation();

  return (
    <View style={[styles.container, containerStyle]}>
      {backBtn ? (
        <TouchableOpacity
          style={styles.left}
          onPress={onBackBtnPress ? onBackBtnPress : () => navigation.goBack()}>
          <Icon
            type="MaterialCommunityIcons"
            name="chevron-left"
            style={styles.backIcon}
          />
        </TouchableOpacity>
      ) : (
        <View style={styles.left} />
      )}

      <View style={styles.wrapTitle}>
        <Text
          style={[
            !subtitleStyle ? appStyles.title : appStyles.subtitle,
            styles.title,
          ]}>
          {title}
        </Text>
        {progress ? <Text style={styles.progress}>{progress}</Text> : <></>}
      </View>

      <View style={styles.right}>{right}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: StatusBar.currentHeight + 10,
    paddingBottom: 10,
    alignItems: 'center',
    borderColor: '#A7A7A7',
    borderBottomWidth: 0.5,
    backgroundColor: colors.lightgray,
    flexDirection: 'row',
  },
  wrapTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  title: {
    textAlign: 'center',
  },
  progress: {
    color: colors.color_2,
    fontSize: 12,
  },
  left: {
    width: 60,
    paddingLeft: 5,
  },
  right: {
    width: 60,
    paddingRight: 10,
    alignItems: 'flex-end',
  },
  backIcon: {
    color: '#4F4F4F',
  },
});

export default PrimaryHeader;
