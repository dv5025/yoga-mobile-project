import React from 'react';
import {
  StyleSheet,
  StatusBar,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import {Icon} from 'native-base';
import {useNavigation} from '@react-navigation/native';
import {colors} from 'src/AppStyles';
import AudioPlayer from './AudioPlayer';

const AccentHeader = ({
  title,
  backBtn,
  rightIcon,
  rightIconOnpress,
  rightIconStyle,
  background,
}) => {
  const navigation = useNavigation();

  const onBack = () => {
    AudioPlayer.stop();
    navigation.goBack();
  };

  return (
    <View
      style={[
        styles.container,
        {backgroundColor: background ? background : colors.color_2},
      ]}>
      <TouchableOpacity
        style={styles.wrapBackIcon}
        onPress={backBtn ? onBack : () => null}>
        {backBtn ? (
          <Icon
            type="MaterialCommunityIcons"
            name="chevron-left"
            style={[styles.icon, styles.backIcon]}
          />
        ) : (
          <></>
        )}
      </TouchableOpacity>
      <View style={styles.wrapTitle}>
        <Text style={styles.title}>{title}</Text>
      </View>
      <TouchableOpacity
        style={styles.wrapRightIcon}
        onPress={rightIconOnpress ? rightIconOnpress : () => null}>
        {rightIcon ? (
          <Icon
            type="MaterialCommunityIcons"
            name={rightIcon}
            style={[styles.icon, rightIconStyle]}
          />
        ) : (
          <></>
        )}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: StatusBar.currentHeight + 5,
    paddingHorizontal: 10,
    paddingBottom: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrapTitle: {
    flex: 1,
  },
  title: {
    color: '#FFFFFF',
    fontSize: 17,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  icon: {
    color: '#FFFFFF',
  },
  wrapBackIcon: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: 60,
  },
  backIcon: {
    fontSize: 35,
  },
  wrapRightIcon: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: 60,
  },
});

export default AccentHeader;
