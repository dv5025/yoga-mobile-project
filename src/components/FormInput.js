import React, {forwardRef, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  inputOnFous,
  inputOnBlur,
} from 'react-native';
import {Icon} from 'native-base';
import {colors} from 'src/AppStyles';

const FormInput = forwardRef(
  (
    {
      label,
      value,
      onChangeText,
      placeholder,
      labelIcon,
      rightIcon,
      rightIconOnPress,
      leftIcon,
      leftIconOnPress,
      round,
      required,
      inputError,
      errorMessage,
      inputTextLabel,
      ...rest
    },
    ref,
  ) => {
    const [isFocus, setIsFocus] = useState(false);

    const onFocus = () => {
      setIsFocus(true);
      if (inputOnFous) {
        inputOnFous();
      }
    };

    const onBlur = () => {
      setIsFocus(false);
      if (inputOnBlur) {
        inputOnBlur();
      }
    };

    return (
      <View style={styles.container}>
        {label ? (
          <View style={styles.wrapLabel}>
            <Icon
              type="MaterialCommunityIcons"
              name={labelIcon}
              style={styles.icon}
            />
            <Text style={styles.label}>{label}</Text>
            {required ? <Text style={styles.required}> *</Text> : <></>}
          </View>
        ) : (
          <></>
        )}
        <View
          style={[
            inputError
              ? styles.inputError
              : isFocus && inputError
              ? styles.inputError
              : isFocus
              ? styles.wrapInputFocus
              : styles.wrapInput,
            round ? styles.round : {},
          ]}>
          {leftIcon ? (
            <TouchableOpacity
              activeOpacity={leftIconOnPress ? 0.5 : 1}
              onPress={leftIconOnPress}>
              <Icon
                type="MaterialCommunityIcons"
                name={leftIcon}
                style={styles.leftIcon}
              />
            </TouchableOpacity>
          ) : (
            <></>
          )}
          <TextInput
            ref={ref}
            {...rest}
            placeholder={placeholder}
            style={styles.input}
            placeholderTextColor="#BBBBBB"
            value={value}
            onChangeText={onChangeText}
            onFocus={onFocus}
            onBlur={onBlur}
          />
          {rightIcon ? (
            <TouchableOpacity
              activeOpacity={rightIconOnPress ? 0.5 : 1}
              onPress={rightIconOnPress}>
              <Icon
                type="MaterialCommunityIcons"
                name={rightIcon}
                style={styles.rightIcon}
              />
            </TouchableOpacity>
          ) : (
            <></>
          )}
        </View>
        {inputError ? (
          <Text style={styles.errorMessage}>{errorMessage}</Text>
        ) : (
          <></>
        )}
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
  },
  wrapLabel: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
  icon: {
    color: '#3B3B3B',
    fontSize: 23,
  },
  label: {
    color: '#3B3B3B',
    marginLeft: 5,
    fontSize: 15,
  },
  wrapInput: {
    borderRadius: 8,
    backgroundColor: '#FCFCFC',
    borderColor: colors.color_1,
    borderWidth: 2,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
  wrapInputFocus: {
    borderRadius: 8,
    backgroundColor: '#FFFFFF',
    borderColor: colors.color_2,
    borderWidth: 2,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
  input: {
    borderRadius: 8,
    fontSize: 13,
    flex: 1,
  },
  inputFocus: {
    borderRadius: 8,
    borderWidth: 1.5,
    borderColor: colors.color_2,
    fontSize: 13,
  },
  rightIcon: {
    color: '#7A7A7A',
    fontSize: 23,
  },
  leftIcon: {
    color: '#7A7A7A',
    fontSize: 23,
    marginRight: 5,
  },
  round: {
    borderRadius: 100,
  },
  required: {
    color: '#D13939',
    fontWeight: 'bold',
  },
  errorMessage: {
    color: '#DA4646',
    marginLeft: 3,
    marginTop: 5,
  },
  inputError: {
    borderRadius: 8,
    backgroundColor: '#FFFFFF',
    borderColor: '#DA4646',
    borderWidth: 2,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
});

export default FormInput;
