import React from 'react';
import {View, StyleSheet} from 'react-native';

const ListDivider = ({width}) => {
  const styles = StyleSheet.create({
    divider: {
      width: '100%',
      height: width ? width : 8,
    },
  });

  return <View style={styles.divider} />;
};

export default ListDivider;
