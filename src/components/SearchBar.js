import React, {useState} from 'react';
import {Input, Item, Icon} from 'native-base';
import {StyleSheet} from 'react-native';

const SearchBar = ({visible, placeholder, containerStyle, ...rest}) => {
  const [isFocus, setIsFocus] = useState(false);

  const onFocus = () => {
    setIsFocus(true);
  };

  const onBlur = () => {
    setIsFocus(false);
  };

  return visible ? (
    <Item
      regular
      style={[
        isFocus ? styles.searchContainerFocus : styles.searchContainer,
        containerStyle,
      ]}>
      <Icon name="ios-search" style={styles.searchIcon} />
      <Input
        {...rest}
        placeholder={placeholder}
        style={styles.searchInput}
        placeholderTextColor="#8E8E92"
        returnKeyType="search"
        onFocus={onFocus}
        onBlur={onBlur}
      />
    </Item>
  ) : (
    <></>
  );
};

const styles = StyleSheet.create({
  searchContainer: {
    backgroundColor: 'rgba(237, 237, 237, 0.9)',
    borderRadius: 8,
    height: 35,
    marginTop: 10,
  },
  searchContainerFocus: {
    backgroundColor: 'rgba(230, 230, 230, 1)',
    borderRadius: 8,
    height: 35,
    marginTop: 10,
  },
  searchInput: {
    fontSize: 12,
  },
  searchIcon: {
    color: '#7F7F84',
    fontSize: 20,
  },
});

export default SearchBar;
