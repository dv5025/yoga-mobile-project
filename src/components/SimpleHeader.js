import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import appStyles from 'src/AppStyles';

const SimpleHeader = ({title, description, right}) => {
  return (
    <View style={styles.container}>
      <View style={appStyles.fulfill}>
        {title ? (
          <View style={styles.titleContainer}>
            <Text style={[appStyles.headline, appStyles.fulfill]}>{title}</Text>
            {right}
          </View>
        ) : (
          <></>
        )}
        {description ? (
          <Text
            style={[styles.headlineDescription, appStyles.headlineDescription]}>
            {description}
          </Text>
        ) : (
          <></>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headlineDescription: {
    marginTop: 5,
  },
});

export default SimpleHeader;
