import React from 'react';
import {View, Text} from 'native-base';
import {StyleSheet, Image} from 'react-native';

const EmptyList = ({label, iconSource}) => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.emptyIcon}
        source={iconSource ? iconSource : require('src/assets/empty-box.png')}
      />
      <Text style={styles.emptyLabel}>{label ? label : 'No data'}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  emptyIcon: {
    width: 90,
    height: 90,
    marginBottom: 10,
    opacity: 0.4,
  },
  emptyLabel: {
    color: '#B2B2B2',
  },
});

export default EmptyList;
