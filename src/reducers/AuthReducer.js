import {LOGIN, LOGOUT} from '../actions/AuthAction';

const initialState = {
  token: '',
  username: '',
  password: '',
  remember: false,
};

export const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        token: action.token,
        username: action.username,
        password: action.password,
        remember: action.remember,
      };
    case LOGOUT:
      return {
        ...state,
        token: '',
      };
    default:
      return state;
  }
};
